﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamAvatarSwitch : MonoBehaviour
{

    public GameObject araC;
    public GameObject phil;
    public GameObject mono6;
    public GameObject major16;

    public bool araCTalking;
    public bool philTalking;
    public bool mono6Talking;
    public bool major16Talking;


    private void Awake()
    {
        araC.SetActive(false);
        phil.SetActive(false);
        mono6.SetActive(false);
        major16.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (araCTalking)
        {
            araC.SetActive(true);
            
            philTalking = false;
            mono6Talking = false;
            major16Talking = false;
        }
        else
        {
            araC.SetActive(false);
        }

        if (philTalking)
        {
            phil.SetActive(true);

            araCTalking = false;
            mono6Talking = false;
            major16Talking = false;
        }
        else
        {
            phil.SetActive(false);
        }

        if (major16Talking)
        {
            major16.SetActive(true);

            philTalking = false;
            araCTalking = false;
            mono6Talking = false;
        }
        else
        {
            major16.SetActive(false);
        }

        
        if(mono6Talking)
        {
            mono6.SetActive(true);

            araCTalking = false;
            philTalking = false;
            major16Talking = false;
        }
        else
        {
            mono6.SetActive(false);
        }


    }
}
