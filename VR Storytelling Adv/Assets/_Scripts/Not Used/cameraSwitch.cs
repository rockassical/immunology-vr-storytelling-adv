﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraSwitch : MonoBehaviour
{
    public Camera CameraAraC;
    public Camera CameraNeutrophil;
    public Camera CameraMac;

    public Material AraC;
    public Material Neutro;
    public Material Mac;

    public GameObject screen;

    public bool cam_ARA;
    public bool cam_Neutrophil;
    public bool cam_Mac;

    private void Update()
    {
        if (cam_ARA)
        {
            ShowCamAraCView();
        }

        if (cam_Neutrophil)
        {
            ShowCamNeutrophilView();
        }

        if (cam_Mac)
        {
            ShowCamMacView();
        }
    }

    // Call this function to disable FPS camera,
    // and enable overhead camera.
    public void ShowCamNeutrophilView()
    {
        screen.GetComponent<Renderer>().material = Neutro;
        CameraAraC.enabled = false;
        CameraNeutrophil.enabled = true;
        CameraMac.enabled = false;
    }

    // Call this function to enable FPS camera,
    // and disable overhead camera.
    public void ShowCamAraCView()
    {
        screen.GetComponent<Renderer>().material = AraC;
        CameraAraC.enabled = true;
        CameraNeutrophil.enabled = false;
        CameraMac.enabled = false;
    }

    public void ShowCamMacView()
    {
        screen.GetComponent<Renderer>().material = Mac;
        CameraAraC.enabled = false;
        CameraNeutrophil.enabled = false;
        CameraMac.enabled = true;
    }
}
