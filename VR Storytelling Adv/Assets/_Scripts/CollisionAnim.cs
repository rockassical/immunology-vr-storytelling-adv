﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionAnim : MonoBehaviour
{
    public Animator anim;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            anim = other.gameObject.GetComponent<Animator>();
            anim.enabled = true;
            this.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
    }
}
