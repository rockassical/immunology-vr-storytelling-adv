﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NETS : MonoBehaviour
{
    //public CellDeathCountdown deathCountDownScript;
    public GameObject player;
    public bool isTriggerDown;

    public GameObject dnaChain_L;
    public GameObject dnaChain_R;

    //public GameObject DNA_L;
    //public GameObject DNA_R;

    public float speed = 0f;
    public float increment = 0.1f;

    Vector3 originalScale;

  
    //Controller setup
    public Transform touchL;
    public bool triggerL;
    public Vector2 stickL;
    public float sensitivityRot = 0.5f;

    public ParticleSystem toxicParticles;
    //public GameObject netsButton;
    //public Material buttonMatActivated;
    //public Material buttonMatOrigin;

    public GameObject aimingReticleNets;
    SpriteRenderer sprite;
    Color originColor;

    WeaponToggleSwitch weaponToggleScript;
    public Automation automationScript;

    private void Awake()
    {
        dnaChain_L.SetActive(false);
        dnaChain_R.SetActive(false);

        //set up aiming reticle color
        sprite = aimingReticleNets.GetComponent<SpriteRenderer>();
        originColor = sprite.color;

        weaponToggleScript = GetComponent<WeaponToggleSwitch>();
    }




    // Start is called before the first frame update
    void Start()
    {
        
    }



    // Update is called once per frame
    void Update()
    {
        //controller input
        touchL.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        touchL.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);

        triggerL = OVRInput.GetDown(OVRInput.RawButton.LIndexTrigger);
        stickL = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);

        //rotate
        if (stickL.x > 0.6f || stickL.x < -0.6f)
        {
           player.transform.Rotate(0, stickL.x * sensitivityRot, 0);
        }

        if (stickL.y > 0.6f || stickL.y < -0.6f)
        {
            player.transform.Rotate(stickL.y * sensitivityRot, 0, 0);
        }

        AimingReticleLock();

        //activation of NETS
        if (triggerL)
        {
            dnaChain_L.SetActive(true);
            dnaChain_R.SetActive(true);

            originalScale = dnaChain_R.transform.localScale;

            isTriggerDown = true;

            weaponToggleScript.isIngestingNow = true;
            weaponToggleScript.isNetsDepolyed = true;

            if (automationScript.isTimerStarted)
            {
                automationScript.isTimerStarted = false;
                automationScript.waitingTime = 60f;
            }
        }

        if(isTriggerDown && speed < 160f && increment != 0f)
        {
            speed += increment;
            dnaChain_L.transform.localScale = originalScale * speed;
            dnaChain_R.transform.localScale = originalScale * speed;

            toxicParticles.Emit(1);
            InflammationControl.crpAmount += 0.05f;
        }
    }


    public void ResetNETS()
    {
        isTriggerDown = false;
        speed = 0f;
        toxicParticles.Emit(0);
        increment = 0.1f;

        dnaChain_L.SetActive(false);
        dnaChain_R.SetActive(false);

        dnaChain_L.transform.localScale = originalScale;
        dnaChain_R.transform.localScale = originalScale;

        weaponToggleScript.isIngestingNow = false;
        weaponToggleScript.isNetsDepolyed = false;
    }


    void AimingReticleLock()
    {
        RaycastHit hit;
        if (Physics.Raycast(player.transform.position, player.transform.forward, out hit, 100f))
        {
            if (hit.collider.tag == "Enemy" || hit.collider.tag == "S A Mutant")
            {
                sprite.color = Color.red;

            }
            else
            {
                sprite.color = originColor;
            }
        }
        else
        {
            sprite.color = originColor;
        }
    }

}
