﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DigitalRuby.LightningBolt;

public class Phagocytosis : MonoBehaviour
{

    public bool isEncapsulated;

    [SerializeField] float alignSpeed;
    public GameObject cockpit;
   
    public LineRenderer selectionLine;
    public Transform endPoint;
    private Transform tempEnd;
    private Vector3 lineEnd;

    public bool startIngestion;

    public GameObject capsule;
    public GameObject pivot;
    public GameObject anchor;
    public GameObject carouselAssem;

    public GameObject storageScale;
    public GameObject bacteriaCapsulePrefeb;
    public GameObject particlesInsidePrefab;

    public ParticleSystem enzymeParticles1;

    public GameObject terminationTextPrefab;

    public GameObject lightningConnectEffect;
    public GameObject lightningEnd;

    public GameObject target;
    private float ingestDist;
    private Vector3 hitObjectCenter;
    private float scaleDownRatio;
    private float scaleDifference;

    private bool isMoving;
    private bool yDone;
    private bool isIngestingNow;

    private RotateWithStops rotateScript;


    //controller setup
    public float sensitivityRot;
    [SerializeField] bool triggerL;
    [SerializeField] bool triggerLUp;
    [SerializeField] bool canRotate = true;
    Vector2 thumbStickL;

    WeaponToggleSwitch weaponToggleScript;

    public bool resetIngestingBug;

    Vector3 tempTarget;

    public bool startAutoIngest;

    public Automation automationScript;
    public GameObject scaleRef;

    private void Awake()
    {
        capsule.SetActive(false);
        
        tempEnd = endPoint;

        rotateScript = pivot.GetComponent<RotateWithStops>();

        weaponToggleScript = GetComponent<WeaponToggleSwitch>();

        lightningConnectEffect.SetActive(false);
    }


    void Start()
    {

    }


    void Update()
    {
        //controller input
        this.transform.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        this.transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);

        triggerL = OVRInput.Get(OVRInput.RawButton.LIndexTrigger);
        thumbStickL = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);
        triggerLUp = OVRInput.GetUp(OVRInput.RawButton.LIndexTrigger);

        ThumbstickRotation();

        if (triggerL)
        {
            selectionLine.enabled = true;
            endPoint = tempEnd;
            lineEnd = endPoint.position;

            selectionLine.SetPosition(0, this.gameObject.transform.position);
            selectionLine.SetPosition(1, lineEnd);

            selectionLine.material.mainTextureOffset = new Vector2(selectionLine.material.mainTextureOffset.x - Random.Range(-0.01f, 0.05f), 0f);

            if (!isIngestingNow)
            {
                RaycastHit hit;
                if (Physics.Raycast(this.gameObject.transform.position, this.gameObject.transform.forward, out hit, 85f))
                {
                    if (hit.collider.tag == "Bacteria" && hit.distance <= 50f)
                    {
                        endPoint = null;
                        selectionLine.SetPosition(1, hit.point);

                        target = hit.collider.gameObject;

                        hitObjectCenter = hit.collider.bounds.center;

                        //show a capsule surrounding selected bacteria
                        capsule.SetActive(true);
                        capsule.transform.position = hit.collider.bounds.center;
                        //capsule.transform.position = target.transform.position;
                        capsule.transform.localScale = hit.collider.bounds.size * 1.5f;
                        //target.transform.parent = capsule.transform;
                        isEncapsulated = true; //(resume timeline audio)

                        if (automationScript.isTimerStarted)
                        {
                            automationScript.isTimerStarted = false;
                            automationScript.waitingTime = 60f;
                        }
                    }

                    if(hit.collider.tag == "S A Mutant")
                    {
                         
                    }
                }
                else
                {
                    lineEnd = endPoint.position;
                }
            }
        }
        else
        {
            selectionLine.enabled = false;
        }

        if (triggerLUp && target != null && target.tag == "Bacteria")
        {
            selectionLine.enabled = false;
            startIngestion = true;

            //lock weapon switch mech when ingestion is happening
            weaponToggleScript.isIngestingNow = true;
            weaponToggleScript.isNetsDepolyed = true;
            canRotate = false;   
        }

        //start auto-phagocytosis in case no user action
        if (startAutoIngest)
        {
            capsule.SetActive(true);
            capsule.transform.position = target.transform.position;
            capsule.transform.localScale = scaleRef.transform.localScale;
            isEncapsulated = true; //(resume timeline audio)

            startIngestion = true;

            //lock weapon switch mech when ingestion is happening
            weaponToggleScript.isIngestingNow = true;
            weaponToggleScript.isNetsDepolyed = true;
            canRotate = false;
        }



        if (startIngestion)
        {
            //connect lightning effect
            lightningEnd.transform.position = capsule.transform.position;
            lightningConnectEffect.SetActive(true);

            isIngestingNow = true;
            
            //calculate distance between bacteria and an anchor point in player
            if (target != null)
            {
                ingestDist = Vector3.Distance(target.transform.position, anchor.transform.position);
                //Debug.Log("Ingestion Dist is " + ingestDist);

            }

            if (!isMoving)
            {
                //start cockpit rotation and movement
                Quaternion r = Quaternion.LookRotation(target.transform.position - cockpit.transform.position);
                Quaternion r2 = Quaternion.Euler(cockpit.transform.rotation.x, r.eulerAngles.y, cockpit.transform.rotation.z);
                cockpit.transform.rotation = Quaternion.RotateTowards(cockpit.transform.rotation, r2, 18f * Time.deltaTime);


                if (cockpit.transform.rotation.Compare(r2, 10 ^ -3))
                {
                    isMoving = true;
                }
            }

            if (isMoving)
            {
                if (!yDone)
                {
                    tempTarget = target.transform.position;
                    tempTarget.y = cockpit.transform.position.y - 3f;

                    alignSpeed = 0.1f;
                    target.transform.position += (tempTarget - target.transform.position).normalized * alignSpeed;
                    capsule.transform.position += (tempTarget - target.transform.position).normalized * alignSpeed;

                    //Debug.Log("tempTarget.y = " + tempTarget.y);
                    //Debug.Log("target.y = " + target.transform.position.y);
                }

                if (Mathf.Abs(target.transform.position.y - tempTarget.y) <= 0.2f)
                {
                    yDone = true;
                    float yDifference = Mathf.Abs(target.transform.position.y - tempTarget.y);
                    //Debug.Log("y difference is" + yDifference);

                    tempTarget.z = cockpit.transform.position.z;
                    tempTarget.x = cockpit.transform.position.x;

                    alignSpeed = 0.2f;
                    target.transform.position += (tempTarget - target.transform.position).normalized * alignSpeed;
                    capsule.transform.position += (tempTarget - target.transform.position).normalized * alignSpeed;
                }

                if (target != null && yDone)
                {
                    scaleDownRatio = 0.1f;
                    scaleDifference = target.transform.localScale.sqrMagnitude - storageScale.transform.localScale.sqrMagnitude;

                    if (scaleDifference > 0f && ingestDist > 3f)
                    {
                        //target.transform.localScale -= (target.transform.localScale - storageScale.transform.localScale) / distOnHit * Time.deltaTime * scaleDownRatio;

                        target.transform.localScale = Vector3.Lerp(target.transform.localScale, storageScale.transform.localScale * 2.5f, scaleDownRatio * Time.deltaTime);
                        capsule.transform.localScale = Vector3.Lerp(capsule.transform.localScale, storageScale.transform.localScale * 2.5f, scaleDownRatio * Time.deltaTime);

                        if (ingestDist <= 3f)
                        {
                            target.transform.localScale = storageScale.transform.localScale;
                            scaleDownRatio = 0f;
                        }
                    }
                }
            }

            //update ingestion dist after movement
            ingestDist = Vector3.Distance(target.transform.position, anchor.transform.position);

            //reset bug in case ingestion gets stuck
            if (resetIngestingBug)
            {
                ingestDist = 0.5f;
                resetIngestingBug = false;
            }

            //if distance between the bacteria and the anchor is less than a set value, the bacteria will snap to the anchor
            if (ingestDist <= 3f && target != null)
            {

                target.transform.position = new Vector3(anchor.transform.position.x, anchor.transform.position.y-1.5f, anchor.transform.position.z); //this -1.5f is to offset bacterium pivot off center, if bacterium pivot is centered, just use "anchor.transform.position" and remove offset in StateTrigger2 script.
                target.tag = "Captured Bacteria";

                target.transform.rotation = storageScale.transform.rotation;

                capsule.SetActive(false);
                lightningConnectEffect.SetActive(false);
                lightningEnd.transform.position = pivot.transform.position;

                GameObject bacteriaCapsule = Instantiate(bacteriaCapsulePrefeb, anchor.transform.position, storageScale.transform.rotation);
                //bacteriaCapsule.transform.localScale = target.transform.localScale * 1.2f;

                target.transform.parent = pivot.transform;
                bacteriaCapsule.transform.parent = pivot.transform;

                Destroy(target.GetComponent<Animator>());

                startIngestion = false;
                canRotate = true;

                //stop carousel rotation
                rotateScript.isRotating = false;

                //show particle enzyme effects
                enzymeParticles1.Play();

                GameObject particlesInside = Instantiate(particlesInsidePrefab, anchor.transform.position, target.transform.rotation);
                particlesInside.transform.localScale = storageScale.transform.localScale * 0.3f;
                particlesInside.transform.parent = bacteriaCapsule.transform;

                //show score text and increase neutrophil life (score)
                GameObject terminationText = Instantiate(terminationTextPrefab, new Vector3(anchor.transform.position.x, anchor.transform.position.y+2f, anchor.transform.position.z + 15f), anchor.transform.rotation);
                Destroy(terminationText, 1f);

                ScoreManager.killCount++;
                InflammationControl.crpAmount -= 20f;

                yDone = false;
                isMoving = false;
                isIngestingNow = false;

                //allow weapon switch mech again
                weaponToggleScript.isIngestingNow = false;
                weaponToggleScript.isNetsDepolyed = false;

                if (startAutoIngest)
                {
                    startAutoIngest = false;
                }
            }
        }
    }

    void ThumbstickRotation()
    {
        if (canRotate)
        {
            if (thumbStickL.x > 0.6f || thumbStickL.x < -0.6f)
            {
                cockpit.transform.Rotate(0, thumbStickL.x * sensitivityRot, 0);
            }

            if (thumbStickL.y > 0.6f || thumbStickL.y < -0.6f)
            {
                cockpit.transform.Rotate(thumbStickL.y * sensitivityRot, 0, 0);
            }
        }
    }

}
