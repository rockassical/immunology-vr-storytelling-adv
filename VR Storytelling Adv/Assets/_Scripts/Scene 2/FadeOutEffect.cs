﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FadeOutEffect : MonoBehaviour
{

    [SerializeField] Renderer[] rendChildren;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //FadeOut();

        //Destroy(this.gameObject, 6f);
    }


    public void FadeOut()
    {
        rendChildren = GetComponentsInChildren<Renderer>();

        foreach (Renderer rend in rendChildren)
        {
            rend.material.DOFade(0.0F, 5F);
        }


    }


}
