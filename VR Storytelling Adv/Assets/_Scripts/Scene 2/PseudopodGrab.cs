﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby;
using DigitalRuby.LightningBolt;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class PseudopodGrab : MonoBehaviour
{
    public GameObject pod;
    public GameObject pseudopod;
    public Transform parent;
    public GameObject nucleus;
    public GameObject refObj;
    public GameObject lightningConnect;
    public GameObject lightningEnd;
    public bool isPseudopodOut;
    public bool startIngestion;
    public float speed = 0.01f;
    public float speedZ = 0.01f;
    public GameObject timeline;
    PlayableDirector pDirector;
    
    //distance adjust
    public GameObject target;
    [SerializeField] float dist2Target;
    public float threshholdGrabDist;
    public float targetMoveSpeed = 0.1f;
    public GameObject pseudopodHead;

    Vector3 obj;
    [SerializeField] float dist;
    [SerializeField] float refDist;
    GameObject bacterium;
    FadeOutEffect fadeScript;
    GameObject lightningBolt;
    LightningBoltScript script;
    Vector3 tempLightningEnd;
    [SerializeField] bool isBacIngestionDone;

    float scaleX;
    float scaleY;
    float scaleZ;

    // Start is called before the first frame update
    void Start()
    {
        scaleX = pod.transform.localScale.x;
        scaleY = pod.transform.localScale.y;
        scaleZ = pod.transform.localScale.z;

        tempLightningEnd = lightningEnd.transform.position;
        lightningConnect.SetActive(false);

        pDirector = timeline.GetComponent<PlayableDirector>();
    }

    // Update is called once per frame
    void Update()
    {
        
        //GrabbingDistanceAdjust();

        if (isPseudopodOut)
        {
            ExtendingPseudopod();

            GrabbingDistanceAdjust();
        }

        if (startIngestion)
        {
            //connect lightning effect
            if (bacterium != null)
            {
                lightningEnd.transform.position = bacterium.transform.position;
                lightningConnect.SetActive(true);
            }


            StartCoroutine(RetractingPseudopod(3f));

        }

        IngestBacteria();



        if (isBacIngestionDone)
        {
            ResumeTimeline();
            isBacIngestionDone = false;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" || other.tag == "S A Mutant Pieces")
        {
            obj = other.gameObject.transform.position;
            //Debug.Log("obj position is" + obj);
            //other.gameObject.transform.parent = pseudopod.transform;
            other.gameObject.transform.SetParent(pseudopod.transform, true);
            bacterium = other.gameObject;
            fadeScript = bacterium.GetComponent<FadeOutEffect>();
            
            startIngestion = true;
            isPseudopodOut = false;


        }
    }


    void IngestBacteria()
    {
        if (obj != null && bacterium != null)
        {
            dist = (nucleus.transform.position - bacterium.transform.position).sqrMagnitude;
            refDist = (nucleus.transform.position - refObj.transform.position).sqrMagnitude;

            //Debug.Log("refDist is" + refDist);
            //Debug.Log("Dist is" + dist);

            if (dist < refDist)
            {
                bacterium.transform.parent = null;
                fadeScript.FadeOut();
                Destroy(bacterium, 6f);
                startIngestion = false;
                StartCoroutine(DisconnectLightning(4f));

            }

        }

    }

    void ExtendingPseudopod()
    {

        if (scaleX <= 3.1f)
        {
            scaleX += speed;
        }

        if (scaleY <= 3.1f)
        {
            scaleY += speed;
        }

        if (scaleZ <= 3.6f)
        {
            scaleZ += speedZ;
        }

        pod.transform.localScale = new Vector3(scaleX, scaleY, scaleZ);
    }

    IEnumerator RetractingPseudopod(float t)
    {
        yield return new WaitForSeconds(t);

        if (scaleX >= 1.01f)
        {
            scaleX -= speed;
        }

        if (scaleY >= 1.01f)
        {
            scaleY -= speed;
        }

        if (scaleZ >= 1.01f)
        {
            scaleZ -= speedZ;
        }

        pod.transform.localScale = new Vector3(scaleX, scaleY, scaleZ);
    }

    IEnumerator DisconnectLightning(float t)
    {
        yield return new WaitForSeconds(t);
        lightningEnd.transform.position = tempLightningEnd;
        lightningConnect.SetActive(false);

        isBacIngestionDone = true;
    }

    void ResumeTimeline()
    {
        if(pDirector.state == PlayState.Paused)
        {
            pDirector.Resume();
        }
    }

    void GrabbingDistanceAdjust()
    {
        dist2Target = Vector3.Distance(target.transform.position, pseudopodHead.transform.position);
        target.transform.position = Vector3.MoveTowards(target.transform.position, pseudopodHead.transform.position, Time.deltaTime * targetMoveSpeed);
        
        if (dist2Target <= threshholdGrabDist)
        {
            targetMoveSpeed = 0f;
        }
        
    }
   
}
