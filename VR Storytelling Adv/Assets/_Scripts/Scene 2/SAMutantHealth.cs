﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class SAMutantHealth : MonoBehaviour
{
    public int healthStart = 200;
    public int healthCurrent;
    public GameObject healthBar;
    public Image sliderImg;
    public Slider slider;
   // [SerializeField] bool isLifeDecreasing;

    public GameObject timeline;

    PlayableDirector pDirector;
    bool isDegranulationHit;
    public bool isNetsHit;
    public bool isTimelineResumed = false;


    // Start is called before the first frame update
    void Start()
    {
        healthBar.SetActive(false);
        healthCurrent = healthStart;
        pDirector = timeline.GetComponent<PlayableDirector>();
    }

    // Update is called once per frame
    void Update()
    {

        if (healthCurrent <= 100 && pDirector.state == PlayState.Paused && !isTimelineResumed)
        {
            pDirector.Resume();
            isTimelineResumed = true;
        }

        if(isNetsHit && pDirector.state == PlayState.Paused)
        {
            pDirector.Resume();
            isNetsHit = false;
        }
/*
        if (isLifeDecreasing)
        {
            slider.value -= Time.deltaTime;

            if(slider.value < 50f)
            {
                slider.value = 50f;
                isLifeDecreasing = false;
            }

            if(slider.value > 100f && slider.value < 150f)
            {
                sliderImg.color = Color.yellow;
            }

            if(slider.value < 100f)
            {
                sliderImg.color = Color.red;
            }

            
        }
*/
    }


    public void DegranulationHit()
    {
        isDegranulationHit = true;

        healthCurrent -= 1;

        healthBar.SetActive(true);

    }

    public void NetsHit()
    {
        isNetsHit = true;
        sliderImg.color = Color.red;

        slider.value = 50f;
        //isLifeDecreasing = true;
    }
}
