﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using SWS;

public class TimelineS2Controls : MonoBehaviour
{

    public ConditionTransitions conditionTransitions;
    public float startTime;
    public splineMove splinePHIL;
    public splineMove splineMAC;
    public int startPoint;
    public int startPointMAC;
    
    PlayableDirector director;
    
    public bool autoStart;
    public bool manualInput; 
    public bool startAfterDegranulation;
    public bool startAfterPhagocytosis;
    public bool startAfterNetsSimulation;

    bool isKillcountChanged;
    bool isKillcountChanged2;

    private void Awake()
    {
        director = GetComponent<PlayableDirector>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (autoStart)
        {
            director.Play();
            splinePHIL.startPoint = 0;
            splinePHIL.StartMove();

        }
        
        
        if (manualInput)
        {
            director.time = startTime;
            director.Play();

            splinePHIL.startPoint = startPoint;
            splinePHIL.StartMove();

            splineMAC.startPoint = startPointMAC;
            splineMAC.StartMove();
        }
        

        if (startAfterDegranulation)
        {
            StartAfterDegranulation();
        }

        if (startAfterPhagocytosis)
        {
            StartAfterPhagocytosis();
        }

        if (startAfterNetsSimulation)
        {
            StartAfterNetsSimulation();
        }
    }

    // Update is called once per frame
    void Update()
    {
       
        if(startAfterDegranulation && !isKillcountChanged)
        {
            ScoreManager.killCount += 1;
            isKillcountChanged = true;
        }

        if (startAfterPhagocytosis && !isKillcountChanged2)
        {
            ScoreManager.killCount += 5;
            isKillcountChanged2 = true;
        }
    }


    void StartAfterDegranulation()
    {
        director.time = 249.04f;
        director.Play();

        splinePHIL.startPoint = 7;
        splinePHIL.StartMove();

        splineMAC.startPoint = 6;
        splineMAC.StartMove();

        //ScoreManager.killCount += 1;
        conditionTransitions.isDegranulationDone = true;
    }

    void StartAfterPhagocytosis()
    {
        director.time = 362.93f;
        director.Play();

        splinePHIL.startPoint = 9;
        splinePHIL.StartMove();

        splineMAC.startPoint = 8;
        splineMAC.StartMove();

        conditionTransitions.isKillcount5 = true;
    }

    void StartAfterNetsSimulation()
    {
        director.time = 476f;
        director.Play();

        splinePHIL.startPoint = 10;
        splinePHIL.StartMove();

        splineMAC.startPoint = 10;
        splineMAC.StartMove();
    }
}
