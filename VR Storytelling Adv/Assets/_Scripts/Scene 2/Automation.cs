﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DigitalRuby.LightningBolt;

public class Automation : MonoBehaviour
{
    public float waitingTime;
    public float timeReset;

    public bool isAutomationOn;
    public bool startAutoDegranulation;
    public bool startAutoPhagocytosis;
    public bool startAutoNETSsim;
    public bool startAutoKill_1stBac;
    public bool startAutoKill_2ndBac;
    public bool startAutoKill_3rdBac;
    public bool startEmit;
    public bool isTimerStarted;
    
    public GameObject cockpit;
    public GameObject targetD;
    public GameObject targetP;
    public GameObject[] bacteria = new GameObject[3];
    public GameObject simTarget;

    public ParticleSystem particleLauncher;
    public ParticleSystem toxicParticles;

    Vector3 turnDirection;
    Vector3 turnBackDirection;
    [SerializeField] float angleDiff;

    //public Degranulation degranulationScript;
    public Phagocytosis phagocytosisScript;
    public NetsSimulation netsSimulationScript;
    public NETS netsScript;

    

    // Start is called before the first frame update
    void Start()
    {
        waitingTime = timeReset;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(ScoreManager.killCount == 0 && !startAutoDegranulation && waitingTime < 0f)
        {
            startAutoDegranulation = true;
            waitingTime = 60f;
            isTimerStarted = false;
        }

        if (ScoreManager.killCount == 1 && !startAutoPhagocytosis && waitingTime < 0f)
        {
            startAutoPhagocytosis = true;
            waitingTime = 60f;
            isTimerStarted = false;
        }

        if (ScoreManager.killCount == 2 && !startAutoKill_1stBac && waitingTime < 0f)
        {
            startAutoKill_1stBac = true;
            waitingTime = 60f;
            isTimerStarted = false;
        }

        if(ScoreManager.killCount == 3 && !startAutoKill_2ndBac)
        {
            startEmit = false;
            StartCoroutine(StartKill2ndBac(5f));
        }

        if (ScoreManager.killCount == 4 && !startAutoKill_3rdBac)
        {
            startEmit = false;
            StartCoroutine(StartKill3rdBac(5f));
        }

        if(ScoreManager.killCount == 5 && !startAutoNETSsim && waitingTime < 0f)
        {
            startAutoNETSsim = true;
        }

        /////////////////////////////////////////////////////////////

        if (startAutoDegranulation)
        {
            isAutomationOn = true;
            TurnToTarget(targetD);
            AutoDegranulation();
        }

        if (startAutoPhagocytosis)
        {
            isAutomationOn = true;
            phagocytosisScript.target = targetP;
            phagocytosisScript.startAutoIngest = true;
        }

        if (startAutoKill_1stBac)
        {
            isAutomationOn = true;
            TurnToTarget(bacteria[0]);
            AutoDegranulation();
        }

        if (startAutoKill_2ndBac)
        {
            TurnToTarget(bacteria[1]);
            AutoDegranulation();
        }


        if (startAutoKill_3rdBac)
        {
            phagocytosisScript.target = bacteria[2];
            phagocytosisScript.startAutoIngest = true;
        }

        if (startAutoNETSsim)
        {
            netsScript.triggerL = true;
            startAutoNETSsim = false;
        }

        if (isTimerStarted)
        {
            //waitingTime = timeReset;

            waitingTime -= Time.deltaTime;
            
        }
    }


    void TurnToTarget(GameObject target)
    {
        if (target != null)
        {
            turnDirection = target.transform.position - cockpit.transform.position;
            Quaternion rotation = Quaternion.LookRotation(turnDirection);
            cockpit.transform.rotation = Quaternion.RotateTowards(cockpit.transform.rotation, rotation, 20f * Time.deltaTime);

            float angle = Quaternion.Angle(cockpit.transform.rotation, rotation);

            if (angle < 1f)
            {
                startEmit = true;
            }
        }

    }

    void AutoDegranulation()
    {

        if (startEmit) 
        {
            particleLauncher.Emit(1);
            toxicParticles.Emit(1);
            InflammationControl.crpAmount += 0.05f;

        }
        else
        {
            particleLauncher.Emit(0);
            toxicParticles.Emit(0);
        }


    }


    IEnumerator StartKill2ndBac(float t)
    {
        yield return new WaitForSeconds(t);
        startAutoKill_2ndBac = true;
    }

    IEnumerator StartKill3rdBac(float t)
    {
        yield return new WaitForSeconds(t);
        startAutoKill_3rdBac = true;
    }


    public void AutomationTimer()
    {
        isTimerStarted = true;
    }
}
