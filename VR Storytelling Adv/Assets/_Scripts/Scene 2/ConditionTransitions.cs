﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using TMPro;
using SWS;

public class ConditionTransitions : MonoBehaviour
{

    public GameObject player;
    public GameObject timelineS2;


    PlayableDirector pDirector;
    splineMove spline;
    public splineMove splineMAC;
    //public WeaponToggleSwitch weaponToggleScript;
    public TimerCountdown timerScript;
    //public Degranulation degranulationScript;
    //public Phagocytosis phagocytosisScript;
    public Automation automationScript;

    //weapon graphics
    public GameObject aimingReticleDegranulation;
    public GameObject aimingReticleNETS;
    public GameObject D_button;
    public GameObject P_button;
    public GameObject N_button;
    public Material buttonUnlit;
    public GameObject weaponInfos;
    public GameObject weaponPropertyGraphic;

    Vector3 turnDirection;
    //bool isMoving;
    public bool isDegranulationDone;
    public bool isPhagocytosisDone;
    public bool isKillcount5;
    public bool isNetsSimulationDone;



    private void Awake()
    {
        pDirector = timelineS2.GetComponent<PlayableDirector>();
        spline = GetComponentInParent<splineMove>();
    }
    // Start is called before the first frame update
    void Start()
    {
        timerScript.isCountDownStart = true;
    }

    // Update is called once per frame
    void Update()
    {

        if ((ScoreManager.killCount == 1 && !isDegranulationDone && pDirector.time > 212f) || (ScoreManager.killCount == 1 && automationScript.isAutomationOn && !isDegranulationDone))
        {
            //turn off automation of degranulation            
            if (automationScript.isAutomationOn)
            {

                automationScript.isAutomationOn = false;

                automationScript.startEmit = false;
                automationScript.startAutoDegranulation = false;
                automationScript.waitingTime = automationScript.timeReset;

                pDirector.time = 212.5f;
                
            }
            

            pDirector.Resume();
            MoveToRoute();
            StartCoroutine(ResumeRouteAfterDegranulation());
            //weaponToggleScript.isUnlocked = false;

            //degranulationScript.isRotationLock = true;
        }



        if ((ScoreManager.killCount == 2 && !isPhagocytosisDone) || (ScoreManager.killCount == 2 && automationScript.isAutomationOn && !isPhagocytosisDone))
        {
            //turn off automation of phagocytosis and reset timer
            if (automationScript.isAutomationOn)
            {
                automationScript.isAutomationOn = false;
                
                automationScript.startAutoPhagocytosis = false;
                automationScript.waitingTime = automationScript.timeReset;

                pDirector.time = 309.5f;
                

            }

            pDirector.Resume();
            MoveToRoute();
            StartCoroutine(ResumeRouteAfterPhagocytosis());


            //weaponToggleScript.isUnlocked = false;
        }


        if ((ScoreManager.killCount == 5 && !isKillcount5) || (ScoreManager.killCount == 5 && automationScript.isAutomationOn && !isKillcount5))
        {
            //turn off automation of phagocytosis
            if (automationScript.isAutomationOn)
            {
                automationScript.startAutoPhagocytosis = false;
                automationScript.waitingTime = automationScript.timeReset;
                automationScript.isAutomationOn = false;

                pDirector.time = 350f;
                pDirector.Resume();


            }
  
            pDirector.Resume();
            MoveToRoute();
            StartCoroutine(ResumeRouteAfterKillcountIs5());


            //weaponToggleScript.isUnlocked = false;
        }

        if (isNetsSimulationDone)
        {
            StartCoroutine(ResumeRouteAfterNetsSimulation());
        }

    }



    void TurnBackToRoute()
    {
        turnDirection = player.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(turnDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 0.5f);

    }

    void MoveToRoute()
    {
        turnDirection = player.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(turnDirection);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 15f * Time.deltaTime);

       // if (transform.rotation.Compare(rotation, 10 ^ -3))
       // {
       //      transform.rotation = player.transform.rotation;
       // }

    }



    IEnumerator ResumeRouteAfterDegranulation()
    {
        yield return new WaitForSeconds(3f);
        spline.Resume();
        splineMAC.Resume();
        yield return new WaitForSeconds(2f);
        WeaponElementsOff();
        weaponInfos.SetActive(false);
        weaponPropertyGraphic.SetActive(false);
        isDegranulationDone = true;

    }

    IEnumerator ResumeRouteAfterPhagocytosis()
    {
        yield return new WaitForSeconds(3f);
        spline.Resume();
        splineMAC.Resume();
        yield return new WaitForSeconds(2f);
        WeaponElementsOff();
        weaponInfos.SetActive(false);
        weaponPropertyGraphic.SetActive(false);
        isPhagocytosisDone = true;
        
    }


    IEnumerator ResumeRouteAfterKillcountIs5()
    {
        yield return new WaitForSeconds(3f);
        spline.Resume();
        splineMAC.Resume();
        yield return new WaitForSeconds(2f);
        WeaponElementsOff();
        weaponInfos.SetActive(false);
        weaponPropertyGraphic.SetActive(false);
        isKillcount5 = true;
        
    }

    IEnumerator ResumeRouteAfterNetsSimulation()
    {
        yield return new WaitForSeconds(2f);
        TurnBackToRoute();
        yield return new WaitForSeconds(2f);
        WeaponElementsOff();
        weaponInfos.SetActive(false);
        weaponPropertyGraphic.SetActive(false);
        isNetsSimulationDone = false;
        
    }


    public void WeaponElementsOff()
    {
        aimingReticleDegranulation.SetActive(false);
        aimingReticleNETS.SetActive(false);

        D_button.GetComponent<Renderer>().material = buttonUnlit;
        P_button.GetComponent<Renderer>().material = buttonUnlit;
        N_button.GetComponent<Renderer>().material = buttonUnlit;
    }



}


