﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class SpeedControl : MonoBehaviour
{
    [SerializeField] GameObject phil;
    public float dist;
    [SerializeField] float actualDist;

    splineMove spline;
    

    private void Awake()
    {
        phil = GameObject.FindGameObjectWithTag("Player");
        spline = GetComponent<splineMove>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        actualDist = Vector3.Distance(phil.transform.position, transform.position);
        //Debug.Log("Distance between MAC and PHIL is " + actualDist);
       // Debug.Log("z distance is " + (transform.position.z - phil.transform.position.z));

        if(actualDist > 30f && actualDist < 70f)
        {
            spline.ChangeSpeed(5f);
        }

        if(actualDist > 70f)
        {
            spline.ChangeSpeed(3f);
        }

    }
}
