﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


enum Weapon { Degranulation, Phagocytosis, Nets };


public class WeaponToggleSwitch : MonoBehaviour
{

    public Transform buttonAssembly;
    public int selectedWeapon = (int)Weapon.Nets;

    public GameObject W1_button;
    public GameObject W2_button;
    public GameObject W3_button;

    public Material buttonLit;
    public Material buttonUnlit;

    Degranulation degranulation;
    Phagocytosis phagocytosis;
    NETS nets;

    //public bool touchPressed = false;

    // weapon info text
    public GameObject weaponInfo;
    public TextMeshPro header;
    public TextMeshPro bodyText;
    //public TextMeshPro animTitle;

    //weapon property graphics
    public GameObject degranulationPropertyBar;
    public GameObject phagocytosisPropertyBar;
    public GameObject netsPropertyBar;
    public GameObject weaponPropertyBarsAssembly;

    public GameObject aimingReticle;
    public GameObject aimingReticleNets;

    public bool isIngestingNow;
    public bool isNetsDepolyed;


    public bool isDegranulationIntroduced;
    public bool isPhagocytosisIntroduced;
    public bool isNetsActivated;
    public bool isDegranulationActivated;
    public bool isPhagocytosisActivated;
    public bool isTwoWeaponToggleActivated;
    public bool isTwoWeaponToggleDone;

    //controller setup
    public Transform touchL;
    bool buttonY;

    //other info text on display
    public GameObject otherInfoOnScreen;
    
    private void Awake()
    {

        degranulation = GetComponent<Degranulation>();
        phagocytosis = GetComponent<Phagocytosis>();
        nets = GetComponent<NETS>();

        degranulation.enabled = false;
        phagocytosis.enabled = false;
        nets.enabled = false;

        aimingReticle.SetActive(false);
        aimingReticleNets.SetActive(false);

        weaponInfo.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        touchL.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        touchL.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);

        buttonY = OVRInput.GetDown(OVRInput.RawButton.Y);

        if (buttonY)
        {

            if (isDegranulationActivated)
            {
                if (!isDegranulationIntroduced)
                {
                    OnButtonPressDegranulation();
                    SelectWeapon();
                }
            }

            if (isPhagocytosisActivated)
            {
                if (!isPhagocytosisIntroduced && !isIngestingNow)
                {
                    OnButtonPressPhagocytosis();
                    SelectWeapon();
                }
            }

            if (isTwoWeaponToggleActivated)
            {
                if (!isTwoWeaponToggleDone && !isIngestingNow)
                {
                    TwoWeaponToggle();
                    SelectWeapon();
                }
            }
                        
           
            if (isNetsActivated)
            {
                if (!isIngestingNow || !isNetsDepolyed)
                {
                    ThreeWeaponToggle();
                    SelectWeapon();
                }
            }
            

        }


        if(ScoreManager.killCount == 1)
        {
            isDegranulationIntroduced = true;
        }

        if (ScoreManager.killCount == 2)
        {
            isPhagocytosisIntroduced = true;
        }

        if (ScoreManager.killCount == 5)
        {
            isTwoWeaponToggleDone = true;
            isNetsActivated = true;
        }

    }


    void SelectWeapon()
    {
        weaponPropertyBarsAssembly.SetActive(true);
        weaponInfo.SetActive(true);

        int i = 0;
        foreach (Transform weapon in buttonAssembly)
        {
            if (i == selectedWeapon)
            {
                if (weapon.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    weapon.gameObject.GetComponent<MeshRenderer>().material = buttonLit;
                }

            }
            else
            {
                if (weapon.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    weapon.gameObject.GetComponent<MeshRenderer>().material = buttonUnlit;
                }

            }

            i++;
        }

        if (selectedWeapon == (int)Weapon.Degranulation)
        {
            degranulation.enabled = true;
            DegranulationInfoText();

            degranulationPropertyBar.SetActive(true);
            aimingReticle.SetActive(true);

            if (otherInfoOnScreen.activeSelf)
            {
                otherInfoOnScreen.SetActive(false);
            }
                        
            //playDegranulationAnim = true;
        }
        else
        {
            degranulation.enabled = false;
            aimingReticle.SetActive(false);
            degranulationPropertyBar.SetActive(false);

            //playDegranulationAnim = false;
        }


        if (selectedWeapon == (int)Weapon.Phagocytosis)
        {
            phagocytosis.enabled = true;
            PhagocytosisInfoText();

            phagocytosisPropertyBar.SetActive(true);

            if (otherInfoOnScreen.activeSelf)
            {
                otherInfoOnScreen.SetActive(false);
            }
            //playPhagocytosisAnim = true;
        }
        else
        {
            phagocytosis.enabled = false;
            phagocytosisPropertyBar.SetActive(false);

            //playPhagocytosisAnim = false;
        }


        if (selectedWeapon == (int)Weapon.Nets)
        {
            nets.enabled = true;
            NetsInfoText();

            netsPropertyBar.SetActive(true);
            aimingReticleNets.SetActive(true);
            //playNetsAnim = true;

        }
        else
        {
            nets.enabled = false;
            netsPropertyBar.SetActive(false);
            aimingReticleNets.SetActive(false);
            //playNetsAnim = false;

        }

    }



    private void ThreeWeaponToggle()
    {


        if (selectedWeapon >= 2)
        {
            selectedWeapon = 0;
        }
        else
        {
            selectedWeapon++;
        }

    }


    private void TwoWeaponToggle()
    {
        if (selectedWeapon >= 1)
        {
            selectedWeapon = 0;
        }
        else
        {
            selectedWeapon++;
        }
    }


    private void OnButtonPressDegranulation()
    {
        if (selectedWeapon >= 0)
        {
            selectedWeapon = 0;
        }
        else
        {
            selectedWeapon++;
        }
    }


    private void OnButtonPressPhagocytosis()
    {
        if (selectedWeapon >= 1)
        {
            selectedWeapon = 1;
        }
        else
        {
            selectedWeapon++;
        }
    }


    public void DegranulationInfoText()
    {
        bodyText.text = "Degranulation Gun Activated!";
        header.text = "[Degranulation]";

        bodyText.color = Color.white;
        header.color = new Color32(255, 78, 0, 255);

        bodyText.fontSize = 0.45f;
        header.fontSize = 0.55f;
        //animTitle.text = "Degranulation";
        
    }

    public void PhagocytosisInfoText()
    {
        bodyText.text = "Phagocytosis Reactor Activated!";
        header.text = "[Phagocytosis]";

        bodyText.color = Color.white;
        header.color = new Color32(255, 78, 0, 255);

        bodyText.fontSize = 0.45f;
        header.fontSize = 0.6f;
        //animTitle.text = "Phagocytosis";
    }

    public void NetsInfoText()
    {
        bodyText.text = "Neutrophil Extracellular Traps(NETS) Activated!";
        header.text = "[NETS]";

        bodyText.color = Color.white;
        header.color = new Color32(255, 78, 0, 255);
        bodyText.fontSize = 0.8f;

        bodyText.fontSize = 0.45f;
        header.fontSize = 0.6f;
        //animTitle.text = "NETS";
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Degranulation Trigger")
        {
            isDegranulationActivated = true;
        }

        if(other.tag == "Phagocytosis Trigger")
        {
            isPhagocytosisActivated = true;
        }

    }



}
