﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanBacteriumMutant : MonoBehaviour
{

    SpriteRenderer rend;

    public bool isColorChange;
    public float interval;
    public bool startScan;
    public float timer;
    public GameObject scanResult;
    public GameObject neutrophilDeactivationInfo;
    public AudioSource popupSound;
    public AudioClip UIsound;
    //public AudioClip scanSound;

    Color originColor;
    float lastTime;
    


    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        originColor = rend.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - lastTime > interval && startScan)
        {
            lastTime = Time.time;
            StartCoroutine(FlickeringColor(interval));
        }

        if (startScan)
        {
            timer -= Time.deltaTime;
        }
        

        if(timer <= 0f)
        {
            startScan = false;
            this.gameObject.SetActive(false);
            neutrophilDeactivationInfo.SetActive(false);
            scanResult.SetActive(true);
            popupSound.PlayOneShot(UIsound, 2f);
        }
        
    }

    IEnumerator FlickeringColor(float time)
    {
        yield return new WaitForSeconds(time);
        isColorChange = !isColorChange;

        if (isColorChange)
        {
            rend.material.color = Color.red;
        }
        else
        {
            rend.material.color = originColor;
        }

    }

    public void StartBacScanning()
    {
        startScan = true;
    }
}
