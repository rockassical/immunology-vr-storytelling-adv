﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetsSimulation : MonoBehaviour
{

    public GameObject sceneSetup;
    public NETS nets;
    
    public GameObject dnaChain_L;
    public GameObject dnaChain_R;

    public Material matHolograph;

    [SerializeField]
    Material[] mat_L;
    [SerializeField]
    Material[] mat_R;
    
    Material matChain;
    Material matRed;
    Material matOrange;
    Material matBlue;
    Material matPurple;

    ConditionTransitions conditionTransitions;

    private void Awake()
    {
        sceneSetup.SetActive(false);
        
        mat_L = dnaChain_L.GetComponent<SkinnedMeshRenderer>().materials;
        mat_R = dnaChain_R.GetComponent<SkinnedMeshRenderer>().materials;

        matChain = mat_L[0];
        matRed = mat_L[1];
        matOrange = mat_L[2];
        matBlue = mat_L[3];
        matPurple = mat_L[4];

        matChain = mat_R[0];
        matRed = mat_R[1];
        matOrange = mat_R[2];
        matBlue = mat_R[3];
        matPurple = mat_R[4];

        conditionTransitions = GetComponent<ConditionTransitions>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void NetsSimulationBegin()
    {
        sceneSetup.SetActive(true);
        
        
        //Change DNA chain materials to holograph
        for(int i = 0; i < mat_L.Length; i++)
        {
            mat_L[i] = matHolograph;
        }

        dnaChain_L.GetComponent<SkinnedMeshRenderer>().materials = mat_L;

        for (int j = 0; j < mat_R.Length; j++)
        {
            mat_R[j] = matHolograph;
        }

        dnaChain_R.GetComponent<SkinnedMeshRenderer>().materials = mat_R;
    }

    public void NetsSimulationEnd()  
    {
        sceneSetup.SetActive(false);
        nets.ResetNETS();
        conditionTransitions.isNetsSimulationDone = true;

        //Restore DNA chain original materials
        mat_L[0] = matChain;
        mat_L[1] = matRed;
        mat_L[2] = matOrange;
        mat_L[3] = matBlue;
        mat_L[4] = matPurple;
        dnaChain_L.GetComponent<SkinnedMeshRenderer>().materials = mat_L;

        mat_R[0] = matChain;
        mat_R[1] = matRed;
        mat_R[2] = matOrange;
        mat_R[3] = matBlue;
        mat_R[4] = matPurple;
        dnaChain_R.GetComponent<SkinnedMeshRenderer>().materials = mat_R;
          
    }

        
}
