﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class SplineMoveBack : MonoBehaviour
{
    [SerializeField] splineMove spline;
    
    // Start is called before the first frame update
    void Start()
    {
        spline = GetComponent<splineMove>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!spline.reverse)
            {
                spline.pathMode = DG.Tweening.PathMode.Ignore;
                spline.Reverse();
            }


        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            if (spline.reverse)
            {
                spline.pathMode = DG.Tweening.PathMode.Full3D;
                spline.Reverse();
            }
        }
    }
}
