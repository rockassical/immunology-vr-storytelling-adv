﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacingCameraOppositeDirection : MonoBehaviour
{
    private GameObject cameraMain;


    void Update()
    {
        cameraMain = GameObject.FindGameObjectWithTag("MainCamera");
        transform.LookAt(-cameraMain.transform.position);
    }
}
