﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class NeutrophilLifeActivationControl : MonoBehaviour
{
    public GameObject neutroLife;
    public GameObject timeline;

    PlayableDirector pDirector;



    private void Awake()
    {
        pDirector = timeline.GetComponent<PlayableDirector>();
        neutroLife.SetActive(false);
    }





    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (pDirector != null)
        {
            if(pDirector.state == PlayState.Paused)
            {
                neutroLife.SetActive(true);
            }
            else
            {
                neutroLife.SetActive(false);
            }
        }
    }
}
