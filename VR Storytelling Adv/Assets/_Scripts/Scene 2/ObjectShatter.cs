﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectShatter : MonoBehaviour
{

    public float expForce, radius;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "S A Mutant Pieces")
        {
            StartCoroutine(ShatterObject());
            //StartCoroutine(ShatterObjectV2());
        }
        
    }


    IEnumerator ShatterObject()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach(Collider col in colliders)
        {
            Rigidbody rb = col.GetComponent<Rigidbody>();

            if(rb != null && col.tag == "S A Mutant Pieces")
            {
                rb.isKinematic = false;
                rb.useGravity = true;
                rb.AddExplosionForce(expForce, transform.position, radius);
                rb.gameObject.GetComponent<TargetMover>().enabled = true;

                yield return new WaitForSeconds(0.1f);
                rb.isKinematic = true;
                rb.useGravity = false;
            }
        }

        if(expForce >= 0f)
        expForce -= 10f;

        if (expForce < 0f)
            expForce = 30;
    }


    IEnumerator ShatterObjectV2()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider col in colliders)
        {
            Rigidbody rb = col.GetComponent<Rigidbody>();

            if (rb != null && col.tag == "S A Mutant Pieces")
            {
                rb.velocity = new Vector3(Random.Range(1f, 5f), Random.Range(8f, 3f), Random.Range(1f, 7f));
            }

            yield return new WaitForSeconds(10f);

            rb.velocity = new Vector3(0f, 0f, 0f);
        }
    }


}
