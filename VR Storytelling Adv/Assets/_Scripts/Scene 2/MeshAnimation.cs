﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshAnimation : MonoBehaviour
{
    private Vector3 center;
    private Vector3[] vertices;
    private Vector3[] normals;
    private Vector3[] verticesoriginal;
    private Matrix4x4 localToWorld;

    private List<List<int>> verticesCorres = new List<List<int>>();
    private int vertlength;
    private Mesh mesh;

    public float scale = 1f;
    void Start()
    {
        center = transform.position;
        mesh = GetComponent<MeshFilter>().mesh;
        verticesoriginal = mesh.vertices;
        localToWorld = transform.localToWorldMatrix;
        //Debug.Log("Mesh Count:" + mesh.vertices.Length);
        //Debug.Log("Triangle Count:" + mesh.triangles.Length);
        vertices = mesh.vertices;
        normals = mesh.normals;
        vertlength = vertices.Length;
        bool[] visited = new bool[vertlength];

        for (var i = 0; i < vertlength; i++)
        {
            if (!visited[i])
            {
                List<int> temp = new List<int>();
                temp.Add(i);
                for (var j = i + 1; j < vertlength; j++)
                {
                    if (vertices[i] == vertices[j])
                    {
                        temp.Add(j);
                        visited[j] = true;
                    }
                }
                verticesCorres.Add(temp);
            }
        }
    }

    void Update()
    {


        for (int i = 0; i + 2 < verticesCorres.Count; i = i + 2)
        {
            for (int j = 0; j < verticesCorres[i].Count; j++)
            {
                vertices[verticesCorres[i][j]] += verticesoriginal[verticesCorres[i][j]] * Mathf.Sin(Time.time) * scale * .001f;
            }
        }

        for (int i = 1; i + 2 < verticesCorres.Count; i = i + 2)
        {
            for (int j = 0; j < verticesCorres[i].Count; j++)
            {
                vertices[verticesCorres[i][j]] += verticesoriginal[verticesCorres[i][j]] * Mathf.Cos(Time.time) * scale * .001f;
            }
        }



        //Matrix4x4 localToWorld = transform.localToWorldMatrix;
        //for (var i = 0; i < vertices.Length; i++)
        //{
        //    //Vector3 world_v = localToWorld.MultiplyPoint3x4(vertices[i]);
        //    //Debug.DrawLine(center, world_v);
        //    //Debug.DrawLine(center, vertices[i]);
        //    //vertices[i] += (verticesoriginal[i] - center) * Mathf.Sin(Time.time) * .01f;
        //    vertices[i] += verticesoriginal[i] * Mathf.Sin(Time.time) * .001f;
        //    for(int j = 0; j < verticesCorres[i].Count; j ++)
        //    vertices[verticesCorres[i][j]] += verticesoriginal[i] * Mathf.Sin(Time.time) * .001f;
        //    //vertices[i + 1] += verticesoriginal[i] * Mathf.Cos(Time.time) * .001f;
        //    //vertices[i + 2] += verticesoriginal[i] * Mathf.Sin(Time.time) * .001f;
        //}

        mesh.vertices = vertices;
    }
}
