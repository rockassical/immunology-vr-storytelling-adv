﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutantResponseEffect : MonoBehaviour
{

    SkinnedMeshRenderer[] rend;
    [SerializeField] [Range(0f, 1f)] float lerpTime;
    [SerializeField] Color[] myColors;
    [SerializeField] int colorIndex = 0;
    [SerializeField] float startTime;
    [SerializeField] float t=0f;

    public float speed;
    public bool changeColor;
    public bool repeatable;
    public bool isNetsHit;

    
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponentsInChildren<SkinnedMeshRenderer>();
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (changeColor)
        {
            ChangeColor(0);
           // Debug.Log("lerp time is" + Time.deltaTime);
        }
    }


    public void ChangeColor(int a)
    {
        if (!repeatable)
        {
            foreach (SkinnedMeshRenderer r in rend)
            {
                r.material.color = Color.Lerp(r.material.color, myColors[a], lerpTime * Time.deltaTime);
            }
        }
        else
        {
            foreach (SkinnedMeshRenderer r in rend)
            {
               
                r.material.color = Color.Lerp(r.material.color, myColors[colorIndex], lerpTime*Time.deltaTime);

                t = Mathf.Lerp(t, 1f, lerpTime * Time.deltaTime);
                if (t > .9f)
                {
                    t = 0f;
                    colorIndex++;
                    colorIndex = (colorIndex >= myColors.Length) ? 0 : colorIndex;


                   // if (colorIndex >= myColors.Length)
                  //  {
                  //      colorIndex = 0;
                  //  }
                }
            }
        }
        
        
    }


    public void ChangeColorRandom()
    {
        if (isNetsHit)
        {
            for( int i=0; i<rend.Length-3; i++)
            {
                int index = Random.Range(0, rend.Length);
                rend[index].material.color = Color.Lerp(myColors[0], myColors[1], lerpTime * Time.deltaTime);

            }
        }
    }
}
