﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ActionTrigger : MonoBehaviour
{
    public PlayableDirector pDirector;
    public splineMove spline;
    public splineMove splineMAC;

    public bool isTimelineResumed_NetSim;
    public bool isTimelineResumed_Phagocytosis;
    public float timer = 1f;
    public bool startCheckingTimeline_NetSim;
    public bool startCheckingTimeline_Phagocytosis;

    BoxCollider col;

    private void Awake()
    {
        col = GetComponent<BoxCollider>();
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (startCheckingTimeline_NetSim)
        {
            CheckTimelineStatus_NetSim();
        }

        if (startCheckingTimeline_Phagocytosis)
        {
            CheckTimelineStatus_PhagocytosisTrigger();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            spline.Pause();

            col.isTrigger = false;
            this.gameObject.SetActive(false);

            if (pDirector.state == PlayState.Paused && this.gameObject.tag == "Degranulation Trigger")
            {
                pDirector.Resume();
               
            }

            if (pDirector.state == PlayState.Paused && this.gameObject.tag == "Phagocytosis Trigger")
            {
                pDirector.Resume();
                isTimelineResumed_Phagocytosis = true;
            }

            if (pDirector.state == PlayState.Playing && this.gameObject.tag == "Phagocytosis Trigger" && !isTimelineResumed_Phagocytosis)
            {
                startCheckingTimeline_Phagocytosis = true;
            }

            
            //activate only degranulation weapon on controller
            if(!other.gameObject.GetComponentInChildren<WeaponToggleSwitch>().isDegranulationIntroduced)
            {
                other.gameObject.GetComponentInChildren<WeaponToggleSwitch>().isDegranulationActivated = true;
            }

            //activate only phagocytosis weapon on controller
            if (!other.gameObject.GetComponentInChildren<WeaponToggleSwitch>().isPhagocytosisIntroduced && other.gameObject.GetComponentInChildren<WeaponToggleSwitch>().isDegranulationIntroduced)
            {
                other.gameObject.GetComponentInChildren<WeaponToggleSwitch>().isPhagocytosisActivated = true;
            }

            //activate both degranulation and phagocytosis on controller
            if(!other.gameObject.GetComponentInChildren<WeaponToggleSwitch>().isTwoWeaponToggleDone && other.gameObject.GetComponentInChildren<WeaponToggleSwitch>().isPhagocytosisIntroduced)
            {
                other.gameObject.GetComponentInChildren<WeaponToggleSwitch>().isTwoWeaponToggleActivated = true;
            }
        }

        if(other.tag == "Mac")
        {
            splineMAC.Pause();
            col.isTrigger = false;
            this.gameObject.SetActive(false);
        }

        if (other.tag == "DNA Chain Collider")
        {
            col.isTrigger = false;

            if (pDirector.state == PlayState.Paused && !isTimelineResumed_NetSim)
            {
                pDirector.Resume();
                isTimelineResumed_NetSim = true;
            }

            if (pDirector.state == PlayState.Playing && !isTimelineResumed_NetSim)
            {
                startCheckingTimeline_NetSim = true;
            }

        }
    }


    void CheckTimelineStatus_PhagocytosisTrigger()
    {
        timer -= Time.deltaTime;

        if (timer < 0f)
        {
            if (pDirector.state == PlayState.Playing)
            {
                timer = 1f;
            }
            else
            {
                startCheckingTimeline_Phagocytosis = false;
                pDirector.Resume();
                isTimelineResumed_Phagocytosis = true;
                timer = 1f;
            }
        }

    }


    void CheckTimelineStatus_NetSim()
    {
        timer -= Time.deltaTime;

        if (timer < 0f)
        {
            if (pDirector.state == PlayState.Playing)
            {
                timer = 1f;
            }
            else
            {
                startCheckingTimeline_NetSim = false;
                pDirector.Resume();
                isTimelineResumed_NetSim = true;
                timer = 1f;
            }
        }

    }


}
