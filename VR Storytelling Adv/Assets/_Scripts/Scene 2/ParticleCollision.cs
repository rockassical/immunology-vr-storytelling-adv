﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollision : MonoBehaviour {

    public bool isBacteriaBoss;
    public ParticleSystem launcher;
    List<ParticleCollisionEvent> collisionEvents;

    private void Start()
    {
        //launcher = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>(); 
    }


    private void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(launcher, other, collisionEvents);

        for(int i = 0; i< collisionEvents.Count; i++)
        {
            if (other.transform.tag == "Bacteria")
            {

                //Debug.Log("bacteria hit");

                if (other.GetComponent<EnemyHealth>())
                {
                    other.GetComponent<EnemyHealth>().GotHit();
                }

            }

            if(other.transform.tag == "S A Mutant")
            {
                Debug.Log("S A Mutant hit");

                if (other.GetComponent<MutantResponseEffect>())
                {
                    other.GetComponent<MutantResponseEffect>().changeColor = true;
                }

                if (other.GetComponent<SAMutantHealth>())
                {
                    other.GetComponent<SAMutantHealth>().DegranulationHit();
                }
            }
            
        }


    }


}
