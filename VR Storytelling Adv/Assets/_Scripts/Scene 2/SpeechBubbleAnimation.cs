﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechBubbleAnimation : MonoBehaviour
{

    //public Transform dots;


    public Material Lit;
    public Material Unlit;

    [SerializeField]
    int selectedDot;
    float lastTime;
    public float interval=0.5f;

    // Start is called before the first frame update
    void Start()
    {
        lastTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if((Time.time - lastTime) > interval)
        {
            lastTime = Time.time;
            LoopDotSelection();
            SelectDot();
        }
    }





    void SelectDot()
    {
        int i = 0;
        foreach(Transform d in transform)
        {
            if(i == selectedDot)
            {
                d.gameObject.gameObject.GetComponent<MeshRenderer>().material = Lit;
            }
            else
            {
                d.gameObject.GetComponent<MeshRenderer>().material = Unlit;
            }

            i++;
        }
    }


    void LoopDotSelection()
    {
        if(selectedDot >= 2)
        {
            selectedDot = 0;
        }
        else
        {
            selectedDot++;
        }
    }
}
