﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class Degranulation : MonoBehaviour
{


    public GameObject player;
    public float sensitivityRot;
    public ParticleSystem particleLauncher;
    public ParticleSystem toxicParticles;

    public bool isLauncherOn = false;
    public InflammationControl inflammation;

    public GameObject aimingReticle;
    //public bool firstLock;
    public bool isRotationLock;
    public bool isTimelineResumed;
    public float timer = 1f;
    public bool startCheckingTimeline;
    public GameObject timeline;
    public Automation automationScript;

    PlayableDirector pDirector;

    SpriteRenderer sprite;
    Color originColor;

    public int raycastHitCounter;

    [SerializeField] bool triggerL;
    Vector2 thumbStickL;

    private void Awake()
    {
        sprite = aimingReticle.GetComponent<SpriteRenderer>();
        originColor = sprite.color;

        pDirector = timeline.GetComponent<PlayableDirector>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        this.transform.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);

        triggerL = OVRInput.Get(OVRInput.RawButton.LIndexTrigger);
        thumbStickL = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);

        AimingReticleLock();


        if (triggerL)
        {
            isLauncherOn = true;

            particleLauncher.Emit(1);
            toxicParticles.Emit(1);

            InflammationControl.crpAmount += 0.05f;

            if (automationScript.isTimerStarted)
            {
                automationScript.isTimerStarted = false;
                automationScript.waitingTime = 60f;
            }
        }
        else
        {
            isLauncherOn = false;
        }


        RotationControl();

        if (startCheckingTimeline)
        {
            CheckTimelineStatus();
        }
    }



    void AimingReticleLock()
    {
        RaycastHit hit;
        if (Physics.Raycast(player.transform.position, player.transform.forward, out hit, 100f))
        {
            if (hit.collider.tag == "Bacteria")
            {
                raycastHitCounter++;

                sprite.color = Color.red;

                if (pDirector.state == PlayState.Paused && !isTimelineResumed)
                {
                    Debug.Log("timeline is paused");
                    isTimelineResumed = true;
                    pDirector.Resume();
                }

                if (pDirector.state == PlayState.Playing && !isTimelineResumed)
                {
                    Debug.Log("timeline is playing");
                    startCheckingTimeline = true;
                }

            }
            else
            {
                sprite.color = originColor;
                startCheckingTimeline = false;
            }
        }
        else
        {
            sprite.color = originColor;
            startCheckingTimeline = false;
        }
    }


    void RotationControl()
    {
        if (!isRotationLock)
        {
            if (thumbStickL.x > 0.6f || thumbStickL.x < -0.6f)
            {
                player.transform.Rotate(0, thumbStickL.x * sensitivityRot, 0);
            }

            if (thumbStickL.y > 0.6f || thumbStickL.y < -0.6f)
            {
                player.transform.Rotate(thumbStickL.y * sensitivityRot, 0, 0);
            }
        }
    }


    void CheckTimelineStatus()
    {
        timer -= Time.deltaTime;

        if (timer < 0f)
        {
            if (pDirector.state == PlayState.Playing)
            {
                timer = 1f;
            }
            else
            {
                timer = 0f;
                startCheckingTimeline = false;
                if (!isTimelineResumed)
                {
                    pDirector.Resume();
                    isTimelineResumed = true;
                }

            }
        }

    }

}
