﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class TurnOffWeaponOnMove : MonoBehaviour
{

    splineMove spline;

    [SerializeField] Degranulation degranulation;
    [SerializeField] Phagocytosis phagocytosis;
    [SerializeField] NETS nets;
    [SerializeField] WeaponToggleSwitch weaponSwitch;

    [SerializeField] Vector3 prePos;
    [SerializeField] Vector3 currPos;

    bool resetWeapons;

    // Start is called before the first frame update
    void Start()
    {
        spline = GetComponent<splineMove>();

        degranulation = GetComponentInChildren<Degranulation>();
        phagocytosis = GetComponentInChildren<Phagocytosis>();
        nets = GetComponentInChildren<NETS>();
        weaponSwitch = GetComponentInChildren<WeaponToggleSwitch>();

        currPos = transform.position;
        prePos = currPos;
        
    }

    // Update is called once per frame
    void Update()
    {
        
        currPos = transform.position;

        //Debug.Log("difference is" + (currPos - prePos).sqrMagnitude);

        
        if (currPos == prePos)
        {

            weaponSwitch.enabled = true;
            resetWeapons = false;

        }
        else
        {
            
            weaponSwitch.enabled = false;
            resetWeapons = true;
        }

        
        if (resetWeapons)
        {
            degranulation.enabled = false;
            phagocytosis.enabled = false;
            nets.enabled = false;
        }


        prePos = currPos;
        
    }
}
