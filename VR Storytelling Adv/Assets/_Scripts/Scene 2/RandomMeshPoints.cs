﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMeshPoints : MonoBehaviour
{
    float r;
    MeshCollider col;
    //[SerializeField] int[] tris;
    public GameObject prefab;
    Vector3 hit;
    public static int numSA=300;
    public GameObject[] allSA = new GameObject[numSA];


    // Start is called before the first frame update
    void Start()
    {
        
        col = GetComponent<MeshCollider>();
        r = col.bounds.max.magnitude;

        //tris = transform.GetComponent<MeshFilter>().mesh.triangles;

        GetRandomPointsOnMesh();
        prefab.transform.localScale = prefab.transform.localScale * 5f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
    void GetRandomPointsOnMesh()
    {
         for(int i=0; i < numSA; i++)
        {
            CalculateSinglePointOnMesh();
            allSA[i] = (GameObject) Instantiate(prefab, hit, Quaternion.identity);
        }
       
    }
    
    void CalculateSinglePointOnMesh()
    {
        Vector3[] meshPoints = transform.GetComponent<MeshFilter>().mesh.vertices;

        int triStart = Random.Range(0, meshPoints.Length / 3) * 3; // get first index of each triangle

        float a = Random.value;
        float b = Random.value;

        if (a + b >= 1)
        { // reflect back if > 1
            a = 1 - a;
            b = 1 - b;
        }

        Vector3 newPointOnMesh = meshPoints[triStart] + (a * (meshPoints[triStart + 1] - meshPoints[triStart])) + (b * (meshPoints[triStart + 2] - meshPoints[triStart])); // apply formula to get new random point inside triangle

        newPointOnMesh = transform.TransformPoint(newPointOnMesh); // convert back to worldspace

        Vector3 rayOrigin = ((Random.onUnitSphere * r) + transform.position); // put the ray randomly around the transform
        Debug.Log("RAY ORIGIN IS " + rayOrigin);

        RaycastHit hitPoint;
        Physics.Raycast(rayOrigin, newPointOnMesh - rayOrigin, out hitPoint, 100f);
        //Debug.DrawRay(rayOrigin, newPointOnMesh - rayOrigin, Color.green, 100f);
        hit = newPointOnMesh;
        hit.y += 2f;
    }


}
