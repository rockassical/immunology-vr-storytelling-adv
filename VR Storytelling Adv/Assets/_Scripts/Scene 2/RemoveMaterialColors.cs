﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RemoveMaterialColors : MonoBehaviour
{


    [SerializeField] Renderer[] rend;
    public Color32 deathColor; //(255, 255, 255, 255)
    private Color32 liveColor;

    
    [SerializeField] [Range(0f, 1f)] float lerpTime;

    [SerializeField] Animator[] anims;

    public bool isCellDeath;

    public Image image;
    public Text crpText;
    public Text bodyTissueInflamm;
    public Image centerCircle;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponentsInChildren<Renderer>();
        anims = GetComponentsInChildren<Animator>();
        //CRPgray.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isCellDeath)
        {
            ChangeColor();
            RemoveAnimation();
            PanelGrayOut();
        }
    }


    public void ChangeColor()
    {

        foreach (Renderer r in rend)
        {
            if (r.material.HasProperty("_Color"))
            {
                liveColor = r.material.color;
                r.material.color = Color32.Lerp(liveColor, deathColor, lerpTime * Time.deltaTime);
            }
        }

       // CRPgray.SetActive(true);
       // CRP.SetActive(false);
        
    }

    void RemoveAnimation()
    {
        foreach(Animator a in anims)
        {
            if(a != null)
            a.speed= 0.5f;
        }

        StartCoroutine(SlowDownAnimation());

    }

    IEnumerator SlowDownAnimation()
    {
        yield return new WaitForSeconds(3);

        foreach(Animator a in anims)
        {
            if (a != null)
            a.speed = 0.2f;
            
        }

        yield return new WaitForSeconds(3);

        foreach (Animator a in anims)
        {
            Destroy(a);
        }

    }

    public void RemoveColors()
    {
        isCellDeath = true;
    }

    public void PanelGrayOut()
    {
        Color32 col = new Color32(180, 180, 180, 255);
        Color32 oldCol;
        oldCol = image.color;
        image.color = Color32.Lerp(oldCol, col, lerpTime * Time.deltaTime);

        //Color32 col2 = new Color32(219, 219, 219, 255);
        //centerCircle.color = col2;

        Color32 colText = new Color32(255, 255, 255, 255);
        Color32 oldTextCol;
        oldTextCol = crpText.color;

        crpText.color = Color32.Lerp(oldTextCol, colText, lerpTime * Time.deltaTime);
        bodyTissueInflamm.color = Color32.Lerp(oldTextCol, colText, lerpTime * Time.deltaTime);
    }

}
