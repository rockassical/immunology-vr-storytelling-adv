﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public GameObject scoreTextPrefab;

    public Renderer[] rend;
    //public Material matDeath;

    public HealthBar healthBar;
    public GameObject healthBarSlider;
    public Image sliderImage;
    public Color32 lowLifeColor;
    public Color32 criticalLifeColor;
   
    //private Color32 deathColor = new Color32(104, 99, 99, 255);
    //private Color32 liveColor;
    //[SerializeField] [Range(0f, 1f)] float lerpTime;

    //private float t;
    public int healthStart;
    //private bool isDead;
    private bool isHit;
    private bool isNetsHit;
    int healthCurrent;
    //private int damage;

    private Quaternion rotation;


    // Use this for initialization
    void Start()
    {
        healthCurrent = healthStart;
        rend = GetComponentsInChildren<Renderer>();

        rotation = Quaternion.identity;

        healthBar.SetMaxHealth(healthStart);
        healthBarSlider.SetActive(false);
    }

    private void Update()
    {
        if (isHit || isNetsHit)
        {

            healthBarSlider.SetActive(true);
            healthBar.SetHealth(healthCurrent);

            foreach (Renderer r in rend)
            {
                /*    if (r.material.HasProperty("_Color"))
                    {
                        liveColor = r.material.color;
                        r.material.color = Color32.Lerp(liveColor, deathColor, lerpTime * Time.deltaTime);
                    }
                 */

                r.material.mainTexture = null;

                if (healthCurrent <= 15)
                {
                    r.material.SetColor("_EmissionColor", Color.black);
                    Destroy(GetComponent<Rotate>());
                    Destroy(GetComponent<Animator>());

                }

            }


            if (healthCurrent <= 15 && healthCurrent > 5)
            {
                sliderImage.color = lowLifeColor;
            }

            if (healthCurrent <= 5)
            {
                sliderImage.color = criticalLifeColor;
            }


        }

    }


    public void GotHit()
    {
        isHit = true;

        healthCurrent -= 1;

             

        if (healthCurrent == 0)
        {

            GameObject scoreText = Instantiate(scoreTextPrefab, transform.position, Quaternion.identity);
            Destroy(scoreText, 1f);

            //ScoreManager.score += 50;
            ScoreManager.killCount++;
            InflammationControl.crpAmount -= 20;

            Destroy(gameObject);

        }

    }

    public void NetsHit()
    {
        isNetsHit = true;

        healthCurrent -= 20;
        //damage = healthStart - healthCurrent;

        GameObject scoreText = Instantiate(scoreTextPrefab, transform.position, transform.rotation);
        Destroy(scoreText, 1f);

        //ScoreManager.score += 50;
        ScoreManager.killCount++;
        InflammationControl.crpAmount -= 20;
    }


    void PlayAudio()
    {
        GameObject obj = GameObject.FindGameObjectWithTag("OVRRig");
        AudioSource audio = obj.GetComponent<AudioSource>();
        
    }
        
}
