﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class SplineBackMoveControl : MonoBehaviour
{
    [SerializeField] splineMove spline;
    public bool moveBack;
    public bool moveForward;

    // Start is called before the first frame update
    void Start()
    {
        spline = GetComponent<splineMove>();
    }

    // Update is called once per frame
    void Update()
    {
        if (moveBack)
        {
            if (!spline.reverse)
            {
                spline.pathMode = DG.Tweening.PathMode.Ignore;
                spline.Reverse();
                spline.StartMove();
              
            }


        }

        if (moveForward)
        {
            if (spline.reverse)
            {
                spline.pathMode = DG.Tweening.PathMode.Full3D;
                spline.Reverse();
            }
        }
    }

    public void SplineMoveBackwards()
    {
        moveBack = true;
    }

}
