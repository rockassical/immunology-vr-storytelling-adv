﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
using TMPro;

public class LookRotationTalking2PHIL : MonoBehaviour
{

    [SerializeField] Transform phil;
    [SerializeField] Transform macOrigin;
    public bool turnToPhil;
    public bool backToRoute;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (turnToPhil)
        {
            TurnToFacePHIL();

        }

        if (backToRoute)
        {
            TurnBackToRoute();
        }
    }

    public void TurnToPHIL()
    {
        turnToPhil = true;
        backToRoute = false;
    }

    public void BackToRoute()
    {
        backToRoute = true;
        turnToPhil = false;
    }


    void TurnToFacePHIL()
    {
        Vector3 directionToFace = phil.position - transform.position;
        Debug.DrawRay(transform.position, directionToFace, Color.green);
        Quaternion targetRotation = Quaternion.LookRotation(directionToFace);

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime);

    }

    void TurnBackToRoute()
    {
        Vector3 directionToTurnback = macOrigin.position - transform.position;
        Debug.DrawRay(transform.position, directionToTurnback, Color.red);
        Quaternion rotationBack = Quaternion.LookRotation(directionToTurnback);
        
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, rotationBack, Time.deltaTime*20f);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotationBack, Time.deltaTime);
   
    }

}
