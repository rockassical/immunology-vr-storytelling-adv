﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Desaturation : MonoBehaviour
{

    public PostProcessVolume volume;
    public bool removeSaturation;
    [SerializeField] float saturValue = 0f;
    
    // Start is called before the first frame update
    void Start()
    {
        volume = GetComponent<PostProcessVolume>();
    }

    // Update is called once per frame
    void Update()
    {
        if (removeSaturation)
        {
            ChangeSaturation();
        }
    }

    public void ChangeSaturation()
    {
        ColorGrading colorGrading;
        volume.profile.TryGetSettings(out colorGrading);
        colorGrading.saturation.value = saturValue;

        if (saturValue > -100f)
        {
            saturValue -= 0.5f;
        }
        
    }

    public void RemoveSaturation()
    {
        removeSaturation = true;
    }
}
