﻿using SWS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngestionTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Mac")
        {
            other.gameObject.GetComponent<splineMove>().Pause();
            
            other.gameObject.GetComponentInChildren<PseudopodGrab>().isPseudopodOut = true;
            this.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
    }

}
