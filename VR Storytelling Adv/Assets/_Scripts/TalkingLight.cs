﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingLight : MonoBehaviour
{

    public GameObject lightSelected;

    Renderer rend;

    //Renderer rend;
    public bool isLightOn;
    public float minTime;
    public float maxTime;
    public float randomTime;

    float lastTime;

    public bool isTalking = false;


    // Start is called before the first frame update
    void Start()
    {

        rend = lightSelected.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
            randomTime = Random.Range(minTime, maxTime);

            if (Time.time - lastTime > randomTime)
            {
                lastTime = Time.time;
                StartCoroutine(FlickeringLight(randomTime));
            }
        
        

        
    }


    IEnumerator FlickeringLight(float time)
    {
        yield return new WaitForSeconds(time);
        isLightOn = !isLightOn;

        if (isLightOn)
        {
            rend.material.EnableKeyword("_EMISSION");
        }
        else
        {
            rend.material.DisableKeyword("_EMISSION");
        }

    }


    
}
