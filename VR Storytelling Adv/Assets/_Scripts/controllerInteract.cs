﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class controllerInteract : MonoBehaviour
{
    public GameObject user;
    public float sensitivityRot=0.5f;
    public float speed;

    public Transform touchL;
    public Transform touchR;

    public bool inputA;
    public bool inputB;
    public bool triggerR;
    public bool triggerL;
    public bool stickL_touched;
    public bool stickR_touched;

    public Vector2 stickR;
    public Vector2 stickL;





    // Update is called once per frame
    void Update()
    {

        touchL.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        touchR.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);

        touchL.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);
        touchR.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);

        inputA = OVRInput.Get(OVRInput.RawButton.A);

        if (OVRInput.GetDown(OVRInput.RawButton.B))
        {
            inputB = !inputB;
        }


        triggerR = OVRInput.Get(OVRInput.RawButton.RIndexTrigger);
        triggerL = OVRInput.Get(OVRInput.RawButton.LIndexTrigger);

        stickR = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
        stickL = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);

        stickL_touched = OVRInput.Get(OVRInput.RawTouch.LThumbstick);
        stickR_touched = OVRInput.Get(OVRInput.RawTouch.RThumbstick);


        //move forward and backward
        user.transform.position += user.transform.forward * Time.deltaTime * stickR.y * speed;
        user.transform.position += user.transform.right * Time.deltaTime * stickR.x * speed;



        //rotate
        if (stickL.x > 0.6f || stickL.x < -0.6f)
        {
            user.transform.Rotate(0, stickL.x * sensitivityRot, 0);
        }



        //move up
        if (stickL.y > 0.6f || stickL.y < -0.6f)
        {
            float height = user.transform.position.y;
            height += Time.deltaTime * stickL.y * speed * 0.5f;

            user.transform.position = new Vector3(user.transform.position.x, height, user.transform.position.z);

        }





    }

}
