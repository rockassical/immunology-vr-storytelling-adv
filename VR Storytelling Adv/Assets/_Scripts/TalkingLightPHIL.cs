﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingLightPHIL : MonoBehaviour
{
    
    [SerializeField]Light talkingLight;

    //Renderer rend;
    public bool isLightOn;
    public float minTime;
    public float maxTime;
    public float randomTime;

    float lastTime;

    public bool isTalking = false;


    // Start is called before the first frame update
    void Start()
    {

        talkingLight = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {

        randomTime = Random.Range(minTime, maxTime);

        if (Time.time - lastTime > randomTime)
        {
            lastTime = Time.time;
            StartCoroutine(FlickeringLight(randomTime));
        }




    }


    IEnumerator FlickeringLight(float time)
    {
        yield return new WaitForSeconds(time);
        isLightOn = !isLightOn;

        if (isLightOn)
        {
            talkingLight.intensity = 6f;
        }
        else
        {
            talkingLight.intensity = 0f;
        }

    }
}
