﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class GetTimelineTimebyWaypoints : MonoBehaviour
{
    public GameObject[] waypoint;
    [SerializeField] PlayableDirector playDierctor;
    public int wNumber1;
    public int wNumber2;
    public int wNumber3;
    bool isTimeRecorded;
    bool isTimeRecorded2;
    bool isTimeRecorded3;


    private void Awake()
    {
        playDierctor = GetComponentInChildren<PlayableDirector>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if((transform.position - waypoint[wNumber1].transform.position).sqrMagnitude <= 0.01f && !isTimeRecorded)
        {
            Debug.Log("Time on waypoint "+ wNumber1 + " is " + playDierctor.time);
            isTimeRecorded = true;
        }

        if ((transform.position - waypoint[wNumber2].transform.position).sqrMagnitude <= 0.01f && !isTimeRecorded2)
        {
            Debug.Log("Time on waypoint " + wNumber2 + " is " + playDierctor.time);
            isTimeRecorded2 = true;
        }

        if ((transform.position - waypoint[wNumber3].transform.position).sqrMagnitude <= 0.01f && !isTimeRecorded3)
        {
            Debug.Log("Time on waypoint " + wNumber3 + " is " + playDierctor.time);
            isTimeRecorded3 = true;
        }
    }
}
