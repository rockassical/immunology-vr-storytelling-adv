﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalLightControl : MonoBehaviour
{

    public Light lightMid;
    public Light lightBottom;

    public float intensity;
    public float range;

    //public ParticleSystem particleRandom;
    public ParticleSystem particleTrails;

    [SerializeField] bool turnupLight;
    [SerializeField] bool turndownLight;
    
    // Start is called before the first frame update
    void Start()
    {
        intensity = lightBottom.intensity;
        range = lightBottom.range;
    }

    // Update is called once per frame
    void Update()
    {
        if (turnupLight)
        {
            TurnUpPortalLights();
            particleTrails.Play();
        }

        if (turndownLight)
        {
            TurnDownPortalLights();
            particleTrails.Stop();
        }
    }


    void TurnUpPortalLights()
    {
        lightBottom.intensity = intensity;
        lightBottom.intensity = range;

        lightMid.intensity = intensity;
        lightMid.range = range;

        intensity += 0.15f;
        if(intensity >= 20f)
        {
            intensity = 20f;
        }

        range += 0.15f;
        if(range >= 30f)
        {
            range = 30f;
        }

    }

    void TurnDownPortalLights()
    {
        lightBottom.intensity = intensity;
        lightBottom.intensity = range;

        lightMid.intensity = intensity;
        lightMid.range = range;

        intensity -= 0.15f;
        if (intensity <= 2f)
        {
            intensity = 2f;
        }

        range -= 0.15f;
        if (range <= 3f)
        {
            range = 3f;
        }
    }

    public void TurnUpLights()
    {
        turndownLight = false;
        turnupLight = true;

    }

    public void TurnDownLights()
    {
        turnupLight = false;
        turndownLight = true;
    }
}
