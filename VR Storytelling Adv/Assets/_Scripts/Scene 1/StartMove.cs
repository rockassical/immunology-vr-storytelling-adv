﻿using SWS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class StartMove : MonoBehaviour
{

    public GameObject mono6;
    private splineMove splineMono6;


    private void Awake()
    {
        splineMono6 = mono6.GetComponent<splineMove>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            splineMono6.StartMove();

            Debug.Log("trigger entered");
        }
    }
}
