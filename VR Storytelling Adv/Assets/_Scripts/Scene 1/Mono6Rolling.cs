﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mono6Rolling : MonoBehaviour
{
    [SerializeField] Animator anim;


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "MONO6")
        {
            anim = other.gameObject.GetComponent<Animator>();
            anim.SetBool("MONO6 starts rolling", true);
            //anim.SetTrigger("Rolling on Surface");

            this.GetComponent<BoxCollider>().isTrigger = false;
        }
    }
}
