﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using JetBrains.Annotations;
using UnityEngine.Playables;
using SWS;

public class ScreenFade : MonoBehaviour
{
    
    public LaserBeam laserBeam;
    public GameObject timeline;
    public GameObject user;
    public GameObject floatingFloor;
    public GameObject cockpit;
    
    private splineMove splinePhil;
    private PlayableDirector pDirector;


    private void Awake()
    {
        splinePhil = user.GetComponent<splineMove>();
        cockpit.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        pDirector = timeline.GetComponent<PlayableDirector>();

        
    }

    // Update is called once per frame
    void Update()
    {
        if (laserBeam.startGame == true)
        {

            floatingFloor.SetActive(false);

            StartCoroutine(ShowCockpit(1.5f));
            //FadeOut();
            pDirector.Play();

            
            laserBeam.startGame = false;

        }


    }


    void FadeIn()
    {
        GetComponent<Renderer>().material.DOFade(1.0F, 5F);

    }

    void FadeOut()
    {
        GetComponent<Renderer>().material.DOFade(0.0F, 5F);
        
    }


    IEnumerator ShowCockpit(float time)
    {
        yield return new WaitForSeconds(time);

        cockpit.SetActive(true);

       // yield return new WaitForSeconds(time);
       // splinePhil.StartMove();
    }
    

}
