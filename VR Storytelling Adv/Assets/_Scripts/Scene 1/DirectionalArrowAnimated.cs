﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalArrowAnimated : MonoBehaviour {

    public GameObject[] arrows;
    public bool startBlinking;

	// Use this for initialization
	void Start () {
        //InvokeRepeating("ArrowAnimation", 1f, 1.6f);
	}

    private void Update()
    {
        if (startBlinking)
        {
            startBlinking = false;
            InvokeRepeating("ArrowAnimation", 1f, 1.6f);

        }
    }


    void ArrowAnimation()
    {
        StartCoroutine(ArrowOnOff(0.2f));
    }


    IEnumerator ArrowOnOff(float second)
    {
        foreach (GameObject a in arrows)
        {
            a.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
            yield return new WaitForSeconds(second);
            a.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
        }
    }
}

