﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class CollisionWithPHIL : MonoBehaviour
{

    [SerializeField] Animator animMONO6;
    [SerializeField] Animator animPHIL;
    [SerializeField] splineMove splineMONO6;
    CellMotions cellMotion;
    public AudioSource audioSource;
    public AudioClip clip;
    public float waitTime=2f;
    public ParticleSystem particleTrails;
    public ParticleSystem particleRandom;

    private void Awake()
    {
        animMONO6 = GetComponent<Animator>();
        splineMONO6 = GetComponentInParent<splineMove>();
        cellMotion = GetComponent<CellMotions>();
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Collided with PHIL!");

            audioSource.PlayOneShot(clip);
            animPHIL = other.gameObject.GetComponent<Animator>();
            animPHIL.SetTrigger("MONO6 Collision");
            //animPHIL.SetBool("PHIL Collision MONO6", true);
            //animMONO6.enabled = true;
            animMONO6.SetTrigger("Collision with PHIL");
            cellMotion.enabled = true;
            StartCoroutine(Mono6ReduceSpeed(waitTime));

            this.gameObject.GetComponent<SphereCollider>().isTrigger = false;
            
        }


        if (other.tag == "MONO6 Rolling Trigger")
        {
            animMONO6.SetBool("MONO6 starts rolling", true);
            other.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }


        if (other.tag == "MONO6 Transmigration")
        {
            animMONO6.SetBool("MONO6 starts rolling", false);
            animMONO6.SetTrigger("MONO6 decending into portal");
            other.gameObject.GetComponent<BoxCollider>().isTrigger = false;

            StartCoroutine(StopParticleTrails(5f));
        }


        if(other.tag == "Play Particle Trails")
        {
            particleTrails.Play();
            particleRandom.Play();
        }

        
    }

   
    public void TurnBackToRoute()
    {
        //animMONO6.SetBool("MONO6 back to route", true);
        animMONO6.SetTrigger("Turns Back to Route");
    }

    IEnumerator Mono6ReduceSpeed(float t)
    {
        yield return new WaitForSeconds(t);
        splineMONO6.ChangeSpeed(7f);
        //animPHIL.SetBool("PHIL Collision MONO6", false);
    }

    IEnumerator StopParticleTrails(float t)
    {
        yield return new WaitForSeconds(t);
        particleTrails.Stop();
        particleRandom.Stop();
    }
}
