﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextLevel : MonoBehaviour {

    public string nextLevel;
    //[SerializeField] Animator anim;
    OVRScreenFade screenFade;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            screenFade = other.gameObject.GetComponentInChildren<OVRScreenFade>();

            if(screenFade != null)
            {
                screenFade.FadeOut();
            }

            StartCoroutine(LoadScene2(3f));
        }

   
    }

    public void LoadtheNextLevel()
    {
        SceneManager.LoadScene(nextLevel);
    }

    IEnumerator LoadScene2(float t)
    {
        yield return new WaitForSeconds(t);
        SceneManager.LoadScene(nextLevel);
    }
}
