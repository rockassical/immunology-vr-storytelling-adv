﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollision : MonoBehaviour
{

    public AudioSource audio;
    public AudioClip clip;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Selectin")
        {
            this.gameObject.GetComponent<Animator>().SetTrigger("PHIL Selectin Hit on Right");
            audio.PlayOneShot(clip);
            
            other.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }

        if(other.tag == "Stop Trigger")
        {
            audio.PlayOneShot(clip);
        }

    }
}
