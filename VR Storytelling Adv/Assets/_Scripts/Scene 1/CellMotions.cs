﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellMotions : MonoBehaviour
{

    public float horizontalMotionMagnitude = 0.05f;
    public float verticalMotionMagnitude = 0.05f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(Vector3.right * Mathf.Cos(Time.timeSinceLevelLoad) * horizontalMotionMagnitude);
        gameObject.transform.Translate(Vector3.up * Mathf.Cos(Time.timeSinceLevelLoad) * verticalMotionMagnitude);
    }
}
