﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ResumeTimelineChemoSensorBeeps : MonoBehaviour
{

    public GameObject timeline;
    public GameObject chemoSensor;

    PlayableDirector playableDirector;
    ChemoSensorAudio chemoAudio;
    [SerializeField] bool isTimelineResumed;
    [SerializeField] bool isTimelineResumed2ndTime;



    private void Awake()
    {
        playableDirector = timeline.GetComponent<PlayableDirector>();
        chemoAudio = chemoSensor.GetComponent<ChemoSensorAudio>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if((chemoAudio.dist >=150f && chemoAudio.dist <= 200f) && isTimelineResumed == false && playableDirector != null)
        {
            playableDirector.Resume();
            isTimelineResumed = true;
        }


        if (chemoAudio.dist < 150f && isTimelineResumed2ndTime == false && isTimelineResumed == true && playableDirector != null)
        {
            playableDirector.Resume();
            isTimelineResumed2ndTime = true;
        }

    }
}
