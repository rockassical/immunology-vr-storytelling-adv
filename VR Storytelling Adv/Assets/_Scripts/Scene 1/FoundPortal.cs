﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class FoundPortal : MonoBehaviour
{

    public GameObject timeline;
    public float timer = 1f;
    public bool startCheckingTimeline;
    public bool isTimelineResumed;

    PlayableDirector playableDirector;

    private void Awake()
    {
        playableDirector = timeline.GetComponent<PlayableDirector>();
    }

    private void Update()
    {
        if (startCheckingTimeline)
        {
            CheckTimelineStatus();
        }
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (playableDirector.state == PlayState.Paused)
            {
                playableDirector.Resume();
                isTimelineResumed = true;
            }


            if (playableDirector.state == PlayState.Playing && !isTimelineResumed)
            {
                startCheckingTimeline = true;
            }

            this.GetComponent<BoxCollider>().isTrigger = false;
        }
    }



    void CheckTimelineStatus()
    {
        timer -= Time.deltaTime;

        if (timer < 0f)
        {
            if(playableDirector.state == PlayState.Playing)
            {
                timer = 1f;
            }
            else
            {
                timer = 0f;
                startCheckingTimeline = false;
                playableDirector.Resume();
            }
        }

    }





}
