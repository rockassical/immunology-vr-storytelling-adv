﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class MuteAnimTrack: MonoBehaviour
{
    public Object player;
    public GameObject timeline;
    PlayableDirector playableDirector;

    private TimelineAsset timelineAsset;


    private void Awake()
    {
        playableDirector = timeline.GetComponent<PlayableDirector>();
    }



    // Start is called before the first frame update
    void Start()
    {
        timelineAsset = (TimelineAsset)playableDirector.playableAsset;
    }




    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            this.GetComponent<BoxCollider>().isTrigger = false;

            playableDirector.Pause();

            MuteTracks(1);
            
            playableDirector.Resume();
        }
    }


    public void MuteTracks(int trackIndex)
    {
        TrackAsset animTrack = timelineAsset.GetOutputTrack(trackIndex);

        animTrack.muted = true;

        //playableDirector.ClearGenericBinding(player);

        double t = playableDirector.time;

        playableDirector.RebuildGraph();

        playableDirector.time = t;
    }


    void RemoveBinding()
    {
        if (playableDirector != null)
        {
            TimelineAsset timelineAsset = (TimelineAsset)playableDirector.playableAsset;
            foreach (PlayableBinding output in timelineAsset.outputs)
            {
                if (output.streamName == "Animation Track")
                {
                    playableDirector.ClearGenericBinding(this.gameObject);
                }
            }

            
        }
    }




}
