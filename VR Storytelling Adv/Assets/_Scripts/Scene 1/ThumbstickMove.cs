﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ThumbstickMove : MonoBehaviour
{
    public Transform touchL;
    public GameObject player;
    public float speed;
    public float sensitivityRot = 0.3f;

    public Vector2 thumbstickL;
    public bool onStick_L;



    // Get move direction indicator materials
    public Material mUnlit;
    public Material mLit;
    public Material mLit2;
    public GameObject indicatorF;
    public GameObject indicatorB;
    public GameObject indicatorR;
    public GameObject indicatorL;
    private Renderer rF;
    private Renderer rB;
    private Renderer rR;
    private Renderer rL;

    // Get animator component
    [SerializeField] Animator anim;
    




    private void Awake()
    {
        rF = indicatorF.GetComponent<Renderer>();
        rB = indicatorB.GetComponent<Renderer>();
        rR = indicatorR.GetComponent<Renderer>();
        rL = indicatorL.GetComponent<Renderer>();

        anim = player.GetComponent<Animator>();

    }



    void Start()
    {
        rF.material = mUnlit;
        rB.material = mUnlit;
        rR.material = mUnlit;
        rL.material = mUnlit;

        //anim = cockPit.GetComponent<Animator>();
    }



    void FixedUpdate()
    {

        touchL.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);

        touchL.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);

        onStick_L = OVRInput.Get(OVRInput.RawTouch.LThumbstick);

        if (onStick_L)
        {
            thumbstickL = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);

            AlignToSurfaceDoubleRay();

            //move forward or backward
            player.transform.position += player.transform.forward * Time.deltaTime * (thumbstickL.y * speed);

            //Forward animation
            if (thumbstickL.y > 0.6f)
            {
                rF.material = mLit;
                anim.SetBool("isForward", true);
            }
            else
            {
                rF.material = mUnlit;
                anim.SetBool("isForward", false);
            }

            //Backward animation
            if (thumbstickL.y < -0.6f)
            {
                rB.material = mLit;
                anim.SetBool("isBack", true);
            }
            else
            {
                rB.material = mUnlit;
                anim.SetBool("isBack", false);
            }


            //rotate
            if (thumbstickL.x > 0.6f || thumbstickL.x < -0.6f)
            {
                player.transform.Rotate(0, thumbstickL.x * sensitivityRot, 0);
            }

            //Rotate right animation
            if (thumbstickL.x > 0.6f)
            {
                rR.material = mLit2;
                anim.SetBool("isRight", true);
            }
            else
            {
                rR.material = mUnlit;
                anim.SetBool("isRight", false);
            }

            //Rotate left animation
            if (thumbstickL.x < -0.6f)
            {
                rL.material = mLit2;
                anim.SetBool("isLeft", true);
            }
            else
            {
                rL.material = mUnlit;
                anim.SetBool("isLeft", false);
            }
        }
        else
        {

            anim.SetBool("isForward", false);
            anim.SetBool("isBack", false);
            anim.SetBool("isLeft", false);
            anim.SetBool("isRight", false);

            rF.material = mUnlit;
            rB.material = mUnlit;
            rL.material = mUnlit;
            rR.material = mUnlit;

        }
    }




    void AlignToSurfaceDoubleRay()
    {
        RaycastHit hitFront;
        Vector3 transformForward = transform.forward;

        RaycastHit hitBack;
        Vector3 transformBack = -transform.forward;

        Ray rayDownFront = new Ray(player.transform.position + 5f * transformForward, -Vector3.up);
        Debug.DrawRay(player.transform.position + 5f * transformForward, -Vector3.up * 50, Color.red);

        Ray rayDownBack = new Ray(player.transform.position + 5f * transformBack, -Vector3.up);
        Debug.DrawRay(player.transform.position + 5f * transformBack, -Vector3.up * 50, Color.red);

        if (Physics.Raycast(rayDownFront, out hitFront, 100f) && Physics.Raycast(rayDownBack, out hitBack, 100f))
        {

            Vector3 hitInfoFront = hitFront.point;
            Vector3 hitInfoBack = hitBack.point;

            Vector3 averageHit = (hitInfoFront + hitInfoBack) / 2;
            Vector3 averageNormal = (hitFront.normal + hitBack.normal) / 2;

            Vector3 temPos = player.transform.position;
            float offset = 8f;
            temPos.y = offset + averageHit.y;
            player.transform.position = temPos;

            Debug.DrawRay(averageHit, averageNormal * 100, Color.green);
            //Debug.Log("temPos.y is "+ temPos.y);

            Quaternion targetRotation = Quaternion.FromToRotation(player.transform.up, averageNormal);
            Quaternion finalRotation = Quaternion.RotateTowards(player.transform.rotation, targetRotation, Time.deltaTime);
            //player.transform.rotation = Quaternion.Euler(finalRotation.eulerAngles.x, 0, 0);
            player.transform.rotation = finalRotation;

            //Debug.Log(player.transform.rotation.x);
        }


    }




}
