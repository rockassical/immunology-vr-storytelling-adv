﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam : MonoBehaviour
{
    public GameObject laserOrigin;
    LineRenderer laser;
    
    public bool triggerR;
    //public bool triggerL;


    public Vector2 stickR;
    //public Vector2 stickL;

    public Vector3 hitPoint;

    public bool startGame;

    private void Start()
    {
        laser = GetComponent<LineRenderer>();
        laser.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        triggerR = OVRInput.Get(OVRInput.RawButton.RIndexTrigger);
        //triggerL = OVRInput.Get(OVRInput.RawButton.LIndexTrigger);

        stickR = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
        //stickL = OVRInput.Get(OVRInput.RawAxis2D.LThumbstick);


        if (triggerR)
        {
            RaycastHit hit;

            if (Physics.Raycast(laserOrigin.transform.position, transform.forward, out hit, 2000))
            {
                hitPoint = hit.point;

                Debug.DrawRay(laserOrigin.transform.position, hitPoint, Color.green);

                laser.enabled = true;

                laser.widthMultiplier = 0.2f;

                laser.SetPosition(0, laserOrigin.transform.position);
                laser.SetPosition(1, hitPoint);

                if (hit.collider.tag == "Start")
                {
                    startGame = true;
                    hit.collider.enabled = false;
                }

            }
            else
            {
                laser.enabled = false;
            }

        }
        else
        {
            laser.enabled = false;
        }

    }
}
