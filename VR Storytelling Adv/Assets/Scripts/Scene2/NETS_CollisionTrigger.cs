﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NETS_CollisionTrigger : MonoBehaviour
{

    public GameObject dnaChainL;
    public GameObject dnaChainR;
    public GameObject lightningPrefab;
    public GameObject rotatingParticlesPrefab;
    private Animator animL;
    private Animator animR;
    private NETS nets;

    private void Awake()
    {

        animL = dnaChainL.GetComponent<Animator>();
        animR = dnaChainR.GetComponent<Animator>();

        nets = GameObject.FindGameObjectWithTag("Left Controller").GetComponent<NETS>();
    }


    private void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.tag == "Simulation Bacteria")
        {
            GameObject target = other.gameObject;
            //other.gameObject.transform.parent = transform;

            animL.SetTrigger("brokenDNA_Slowdown");
            animR.SetTrigger("fullDNA_Slowdown");

            GameObject particleEffect = Instantiate(lightningPrefab, target.transform.position, Quaternion.identity);
            particleEffect.transform.localScale = particleEffect.transform.localScale * 3f;

            other.GetComponent<Collider>().enabled = false;

            nets.increment = 0f;

        }


        if (other.gameObject.tag == "S A Mutant")
        {
            GameObject target = other.gameObject;
            
            animL.SetTrigger("brokenDNA_Slowdown");
            animR.SetTrigger("fullDNA_Slowdown");

            GameObject particleEffect = Instantiate(lightningPrefab, target.transform.position, Quaternion.identity);
            particleEffect.transform.localScale = particleEffect.transform.localScale * 3f;

            GameObject movingParticles = Instantiate(rotatingParticlesPrefab, target.transform.position, Quaternion.identity);

            ParticleSystem toxicParticles = target.GetComponentInChildren<ParticleSystem>();
            if (toxicParticles != null)
            {
                var emission = toxicParticles.emission;
                emission.rateOverTime = 1f;
            }


            Animator anim = target.GetComponent<Animator>();
            if (anim != null)
            {
                anim.SetBool("Nets hit", true);
            }

            other.GetComponent<Collider>().enabled = false;

            nets.increment = 0f;
            //nets.toxicParticles.Emit(0);
            other.GetComponent<SAMutantHealth>().NetsHit();
        }
    }
}
