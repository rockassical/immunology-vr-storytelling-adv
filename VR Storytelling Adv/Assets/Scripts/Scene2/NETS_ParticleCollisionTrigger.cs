﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NETS_ParticleCollisionTrigger : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bacteria")
        {
            GameObject target = other.gameObject;
            
            target.GetComponent<EnemyHealth>().GotHit();
        }

        
        
    }
}
