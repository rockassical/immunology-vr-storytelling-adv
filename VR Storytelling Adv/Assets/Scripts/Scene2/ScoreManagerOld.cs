﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManagerOld : MonoBehaviour {


    public static int score;
    public int scoreRead;

    public static int killCount;

    private int tempHealth;
    private Text text;

    public TextMeshPro killCountText;

    // Use this for initialization
    void Start () {

        text = GetComponent<Text>();
        scoreRead = score;
        score = 0;
        killCount = 0;
    }
	
	// Update is called once per frame
	void Update () {

        tempHealth = (int)PlayerHealth.CurrentHealth;
        text.text = "Host Health: " + (tempHealth + score);
        scoreRead = score;

        if (killCountText != null)
        {
            killCountText.text = "Enemy Killed:  " + killCount;
        }
    }
}
